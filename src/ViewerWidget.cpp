#include   "ViewerWidget.h"

ViewerWidget::ViewerWidget(QString viewerName, QSize imgSize, QWidget* parent)
	: QWidget(parent)
{
	setAttribute(Qt::WA_StaticContents);
	setMouseTracking(true);
	name = viewerName;
	if (imgSize != QSize(0, 0)) {
		img = new QImage(imgSize, QImage::Format_ARGB32);
		img->fill(Qt::white);
		resizeWidget(img->size());
		setPainter();
		setDataPtr();
	}
}
ViewerWidget::~ViewerWidget()
{
	delete painter;
	delete img;
}
void ViewerWidget::resizeWidget(QSize size)
{
	this->resize(size);
	this->setMinimumSize(size);
	this->setMaximumSize(size);
}

void ViewerWidget::resizeImg(int width, int height)
{
	QImage::Format format = img->format();
	img = new QImage(width,height, format );
	img->fill(Qt::white);
	resizeWidget(img->size());
	setPainter();
}

//Image functions
bool ViewerWidget::setImage(const QImage& inputImg)
{
	if (img != nullptr) {
		delete img;
	}
	img = new QImage(inputImg);
	if (!img) {
		return false;
	}
	resizeWidget(img->size());
	setPainter();
	setDataPtr();
	return true;
}

void ViewerWidget::setPixel(QImage* img, int x, int y, uchar r, uchar g, uchar b, uchar a)
{
	uchar* d = img->bits();
	if (isInside(img, x, y)) {
		int startbyte = y * img->bytesPerLine() + x * 4;
		d[startbyte] = r;
		d[startbyte + 1] = g;
		d[startbyte + 2] = b;
		d[startbyte + 3] = a;
	}
}
void ViewerWidget::setPixel(QImage* img, int x, int y, uchar val)
{
	uchar* d = img->bits();
	if (isInside(img, x, y)) {
		d[y * img->bytesPerLine() + x] = val;
	}
}
void ViewerWidget::setPixel(QImage* img, int x, int y, double val)
{
	if (isInside(img, x, y)) {
		if (val > 1) val = 1;
		if (val < 0) val = 0;
		setPixel(img, x, y, static_cast<uchar>(255 * val));
	}
}
void ViewerWidget::setPixel(QImage* img, int x, int y, double valR, double valG, double valB, double valA)
{
	if (isInside(img, x, y)) {
		if (valR > 1) valR = 1;
		if (valG > 1) valG = 1;
		if (valB > 1) valB = 1;

		if (valR < 0) valR = 0;
		if (valG < 0) valG = 0;
		if (valB < 0) valB = 0;

		double newValA = valA;
		if (newValA > 1) newValA = 1;
		if (newValA < 0) newValA = 0;
		setPixel(img, x, y, static_cast<uchar>(255 * valR), static_cast<uchar>(255 * valG), static_cast<uchar>(255 * valB), static_cast<uchar>(255 * valA));
	}
}

void ViewerWidget::calcHistogramBW()
{
	int row = img->bytesPerLine();

	histogram.resize(256);
	for (int i = 0; i <= 255; i++)
		histogram[i] = 0;

	for (int i = 0; i < getImgHeight(); i++) {
		for (int j = 0; j < getImgWidth(); j++) {
			int h = static_cast<uchar>(data[i * row + j]);
			histogram[h]++;
		}
	}
}

double ViewerWidget::getMeanBW()
{
	double sum = 0;
	for (int i = 0; i < 256; i++)
		sum += histogram[i] * i;

	return sum / (double)(getImgHeight() * getImgWidth());
}

void ViewerWidget::calcHistogramRGB()
{
	int row = img->bytesPerLine();

	histogramR.resize(256);
	histogramG.resize(256);
	histogramB.resize(256);
	for (int i = 0; i <= 255; i++) {
		histogramR[i] = 0;
		histogramG[i] = 0;
		histogramB[i] = 0;
	}

	for (int i = 0; i < getImgHeight(); i++) {
		for (int j = 0; j < getImgWidth(); j++) {
			int r = static_cast<uchar>(data[i * row + j * 4]);
			int g = static_cast<uchar>(data[i * row + j * 4 + 1]);
			int b = static_cast<uchar>(data[i * row + j * 4 + 2]);
			histogramR[r]++;
			histogramG[g]++;
			histogramB[b]++;
		}
	}
}

//Draw functions
void ViewerWidget::freeDraw(const QPoint& end, const QPen& pen)
{
	painter->setPen(pen);
	painter->drawLine(freeDrawBegin, end);
	update();
}

void ViewerWidget::saveIteration()
{
	imgList << *img;
}

void ViewerWidget::clearImgList()
{
	imgList.clear();
	imgList << *img;
}

void ViewerWidget::showImgFromList(int i)
{
	img = new QImage(imgList.at(i));
	setDataPtr();
	update();
}

//Slots
void ViewerWidget::paintEvent(QPaintEvent* event)
{
	QPainter painter(this);
	QRect area = event->rect();
	painter.drawImage(area, *img, area);
}