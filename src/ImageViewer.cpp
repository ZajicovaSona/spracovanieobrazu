#include "ImageViewer.h"

ImageViewer::ImageViewer(QWidget* parent)
	: QMainWindow(parent), ui(new Ui::ImageViewerClass)
{
	ui->setupUi(this);
	ui->loadingLabel->setHidden(true);
	ui->horizontalSlider->setHidden(true);
	//ui->dockWidget->setHidden(true);
}

//ViewerWidget functions
ViewerWidget* ImageViewer::getViewerWidget(int tabId)
{
	QScrollArea* s = static_cast<QScrollArea*>(ui->tabWidget->widget(tabId));
	if (s) {
		ViewerWidget* vW = static_cast<ViewerWidget*>(s->widget());
		return vW;
	}
	return nullptr;
}
ViewerWidget* ImageViewer::getCurrentViewerWidget()
{
	return getViewerWidget(ui->tabWidget->currentIndex());
}

// Event filters
bool ImageViewer::eventFilter(QObject* obj, QEvent* event)
{
	if (obj->objectName() == "ViewerWidget") {
		return ViewerWidgetEventFilter(obj, event);
	}
	return false;
}

//ViewerWidget Events
bool ImageViewer::ViewerWidgetEventFilter(QObject* obj, QEvent* event)
{
	ViewerWidget* w = static_cast<ViewerWidget*>(obj);

	if (!w) {
		return false;
	}

	if (event->type() == QEvent::MouseButtonPress) {
		ViewerWidgetMouseButtonPress(w, event);
	}
	else if (event->type() == QEvent::MouseButtonRelease) {
		ViewerWidgetMouseButtonRelease(w, event);
	}
	else if (event->type() == QEvent::MouseMove) {
		ViewerWidgetMouseMove(w, event);
	}
	else if (event->type() == QEvent::Leave) {
		ViewerWidgetLeave(w, event);
	}
	else if (event->type() == QEvent::Enter) {
		ViewerWidgetEnter(w, event);
	}
	else if (event->type() == QEvent::Wheel) {
		ViewerWidgetWheel(w, event);
	}

	return QObject::eventFilter(obj, event);
}
void ImageViewer::ViewerWidgetMouseButtonPress(ViewerWidget* w, QEvent* event)
{
	QMouseEvent* e = static_cast<QMouseEvent*>(event);
	if (e->button() == Qt::LeftButton) {
		w->setFreeDrawBegin(e->pos());
		w->setFreeDrawActivated(true);
	}
}
void ImageViewer::ViewerWidgetMouseButtonRelease(ViewerWidget* w, QEvent* event)
{
	QMouseEvent* e = static_cast<QMouseEvent*>(event);
	if (e->button() == Qt::LeftButton && w->getFreeDrawActivated()) {
		w->freeDraw(e->pos(), QPen(Qt::red));
		w->setFreeDrawActivated(false);
	}
}
void ImageViewer::ViewerWidgetMouseMove(ViewerWidget* w, QEvent* event)
{
	QMouseEvent* e = static_cast<QMouseEvent*>(event);
	if (e->buttons() == Qt::LeftButton && w->getFreeDrawActivated()) {
		w->freeDraw(e->pos(), QPen(Qt::red));
		w->setFreeDrawBegin(e->pos());
	}
}
void ImageViewer::ViewerWidgetLeave(ViewerWidget* w, QEvent* event)
{
}
void ImageViewer::ViewerWidgetEnter(ViewerWidget* w, QEvent* event)
{
}
void ImageViewer::ViewerWidgetWheel(ViewerWidget* w, QEvent* event)
{
	QWheelEvent* wheelEvent = static_cast<QWheelEvent*>(event);
}

//ImageViewer Events
void ImageViewer::closeEvent(QCloseEvent* event)
{
	if (QMessageBox::Yes == QMessageBox::question(this, "Close Confirmation", "Are you sure you want to exit?", QMessageBox::Yes | QMessageBox::No))
	{
		event->accept();
	}
	else {
		event->ignore();
	}
}

//Image functions
void ImageViewer::openNewTabForImg(ViewerWidget* vW)
{
	QScrollArea* scrollArea = new QScrollArea;
	scrollArea->setObjectName("QScrollArea");
	scrollArea->setWidget(vW);

	scrollArea->setBackgroundRole(QPalette::Dark);
	scrollArea->setWidgetResizable(true);
	scrollArea->installEventFilter(this);
	scrollArea->setHorizontalScrollBarPolicy(Qt::ScrollBarAsNeeded);
	scrollArea->setVerticalScrollBarPolicy(Qt::ScrollBarAsNeeded);

	vW->setObjectName("ViewerWidget");
	vW->installEventFilter(this);

	QString name = vW->getName();

	ui->tabWidget->addTab(scrollArea, name);
}
bool ImageViewer::openImage(QString filename)
{
	QFileInfo fi(filename);

	QString name = fi.baseName();
	openNewTabForImg(new ViewerWidget(name, QSize(0, 0)));
	ui->tabWidget->setCurrentIndex(ui->tabWidget->count() - 1);
	tabCount++;

	ViewerWidget* w = getCurrentViewerWidget();

	QImage loadedImg(filename);
	return w->setImage(loadedImg);
}
bool ImageViewer::saveImage(QString filename)
{
	QFileInfo fi(filename);
	QString extension = fi.completeSuffix();
	ViewerWidget* w = getCurrentViewerWidget();

	QImage* img = w->getImage();
	return img->save(filename, extension.toStdString().c_str());
}
bool ImageViewer::clearImage()
{
	ViewerWidget* w = getCurrentViewerWidget();
	w->clear();
	w->update();
	return true;
}
bool ImageViewer::invertColors()
{
	ViewerWidget* w = getCurrentViewerWidget();
	uchar* data = w->getData();
	int row = w->getImage()->bytesPerLine();
	int depth = w->getImage()->depth();

	for (int i = 0; i < w->getImgHeight(); i++){
		for (int j = 0; j < w->getImgWidth(); j++){
			if (depth == 8) {
				w->setPixel(w->getImage(), j, i, static_cast<uchar>(255 - data[i * row + j]));
			}
			else {
				uchar r = static_cast<uchar>(255 - data[i * row + j * 4]);
				uchar g = static_cast<uchar>(255 - data[i * row + j * 4 + 1]);
				uchar b = static_cast<uchar>(255 - data[i * row + j * 4 + 2]);
				w->setPixel(w->getImage(), j, i, r, g, b);
			}
		}
	}

	w->update();
	return true;
}
QVector<int> ImageViewer::mirroring(int k)
{
	ViewerWidget* w = getCurrentViewerWidget();
	uchar* data = w->getData();
	QVector<int> d;
	int depth = w->getImage()->depth();
	int row = w->getImage()->bytesPerLine();
	int height = w->getImgHeight();
	int width = w->getImgWidth();
	int rowNew;
	
	if (depth == 8) {
		if (k % 2 == 0) {
			rowNew = row + k * 2;
			d.resize((width + 2 * k)*(height + 2 * k));
		}
		else {
			rowNew = row + (k + 1) * 2;
			d.resize((width + 2 * (k + 1))*(height + 2 * (k + 1)));
		}
		for (int step = 0; step < k; step++) {
			//top: i=k-1...0   j=k...k+width
			//bottom:  i = k+height...2k+height  j = k...k+width
			for (int j = k; j < k + width; j++) {
				d[(k - 1 - step) * rowNew + j] = data[step * row + (j - k)];						//top
				d[(k + height + step) * rowNew + j] = data[(height - 1 - step) * row + (j - k)];	//bottom
			}
			//left: j = k-1...0  i = k...k+height
			//right: j= k+width...2k+width  i = k...k+height
			for (int i = k; i < k + height; i++) {
				d[i * rowNew + (k - 1 - step)] = data[(i - k) * row + step];						//left
				d[i * rowNew + (k + width + step)] = data[(i - k) * row + (width - 1 - step)];		//right
			}
		}
		for (int i = k - 1; i >= 0; i--)															//top-left
			for (int j = k - 1; j >= 0; j--)
				d[i * rowNew + j] = data[(k - 1 - i) * row + (k - 1 - j)];
		for (int i = k + height; i < 2 * k + height; i++) 											//bottom-right
			for (int j = k + width; j < 2 * k + width; j++)
				d[i * rowNew + j] = data[(2 * height - i + k - 1) * row + (2 * width - j + k - 1)];
		for (int i = k - 1; i >= 0; i--)															//top-righ
			for (int j = k + width; j < 2 * k + width; j++)
				d[i * rowNew + j] = data[(k - 1 - i) * row + (2 * width - j + k - 1)];
		for (int i = k + height; i < 2 * k + height; i++)											//bottom-left
			for (int j = k - 1; j >= 0; j--)
				d[i * rowNew + j] = data[(2 * height - i + k - 1) * row + (k - 1 - j)];
		for (int i = k; i < k + height; i++)														//inside
			for (int j = k; j < k + width; j++)
				d[i * rowNew + j] = data[(i - k) * row + (j - k)];
	}
	else {	//RGB
		if (k % 2 == 0) {
			rowNew = row + k * 2 * 3;
			d.resize((height + 2 * k) * rowNew + (width + 2 * k) * 3 + 3);
		}
		else {
			rowNew = row + (k + 1) * 2 * 3;
			d.resize((height + 2 * (k + 1)) * rowNew + (width + 2 *(k + 1) ) * 3 + 3);
		}
		d.fill(255);
		for (int step = 0; step < k; step++) {
			for (int j = k; j < k + width; j++) {
				d[(k - 1 - step) * rowNew + j * 3] = data[step * row + (j - k) * 4];						//top
				d[(k - 1 - step) * rowNew + j * 3 + 1] = data[step * row + (j - k) * 4 + 1];
				d[(k - 1 - step) * rowNew + j * 3 + 2] = data[step * row + (j - k) * 4 + 2];
				d[(k + height + step) * rowNew + j * 3] = data[(height - 1 - step) * row + (j - k) * 4];	//bottom
				d[(k + height + step) * rowNew + j * 3 + 1] = data[(height - 1 - step) * row + (j - k) * 4 + 1];
				d[(k + height + step) * rowNew + j * 3 + 2] = data[(height - 1 - step) * row + (j - k) * 4 + 2];
			}
			for (int i = k; i < k + height; i++) {
				d[i * rowNew + (k - 1 - step) * 3] = data[(i - k) * row + step * 4];						//left
				d[i * rowNew + (k - 1 - step) * 3 + 1] = data[(i - k) * row + step * 4 + 1];
				d[i * rowNew + (k - 1 - step) * 3 + 2] = data[(i - k) * row + step * 4 + 2];
				d[i * rowNew + (k + width + step) * 3] = data[(i - k) * row + (width - 1 - step) * 4];		//right
				d[i * rowNew + (k + width + step) * 3 + 1] = data[(i - k) * row + (width - 1 - step) * 4 + 1];
				d[i * rowNew + (k + width + step) * 3 + 2] = data[(i - k) * row + (width - 1 - step) * 4 + 2];
			}
		}
		for (int i = k - 1; i >= 0; i--)															//top-left
			for (int j = k - 1; j >= 0; j--) {
				d[i * rowNew + j * 3] = data[(k - 1 - i) * row + (k - 1 - j )* 4];
				d[i * rowNew + j * 3 + 1] = data[(k - 1 - i) * row + (k - 1 - j )* 4 + 1];
				d[i * rowNew + j * 3 + 2] = data[(k - 1 - i) * row + (k - 1 - j )* 4 + 2];
			}
		for (int i = k + height; i < 2 * k + height; i++) 											//bottom-right
			for (int j = k + width; j < 2 * k + width; j++) {
				d[i * rowNew + j * 3] = data[(2 * height - i + k - 1) * row + (2 * width - j + k - 1) * 4];
				d[i * rowNew + j * 3 + 1] = data[(2 * height - i + k - 1) * row + (2 * width - j + k - 1) * 4 + 1];
				d[i * rowNew + j * 3 + 2] = data[(2 * height - i + k - 1) * row + (2 * width - j + k - 1) * 4 + 2];
			}
		for (int i = k - 1; i >= 0; i--)															//top-righ
			for (int j = k + width; j < 2 * k + width; j++) {
				d[i * rowNew + j * 3] = data[(k - 1 - i) * row + (2 * width - j + k - 1) * 4];
				d[i * rowNew + j * 3 + 1] = data[(k - 1 - i) * row + (2 * width - j + k - 1) * 4 + 1];
				d[i * rowNew + j * 3 + 2] = data[(k - 1 - i) * row + (2 * width - j + k - 1) * 4 + 2];
			}
		for (int i = k + height; i < 2 * k + height; i++)											//bottom-left
			for (int j = 0 ; j < k ; j++) {
				d[i * rowNew + j * 3] = data[(2 * height - i + k - 1) * row + (k - 1 - j) * 4];
				d[i * rowNew + j * 3 + 1] = data[(2 * height - i + k - 1) * row + (k - 1 - j) * 4 + 1];
				d[i * rowNew + j * 3 + 2] = data[(2 * height - i + k - 1) * row + (k - 1 - j) * 4 + 2];
			}
		for (int i = k; i < k + height; i++)														//inside
			for (int j = k; j < k + width; j++) {
				d[i * rowNew + j * 3] = data[(i - k) * row + (j - k) * 4];
				d[i * rowNew + j * 3 + 1] = data[(i - k) * row + (j - k) * 4 + 1];
				d[i * rowNew + j * 3 + 2] = data[(i - k) * row + (j - k) * 4 + 2];
			}
	}
	
	return d;
}
bool ImageViewer::FSHS()
{
	ViewerWidget* w = getCurrentViewerWidget();
	uchar* data = w->getData();
	int row = w->getImage()->bytesPerLine();
	int depth = w->getImage()->depth();
	
	if (depth == 8) {
		w->calcHistogramBW();
		QVector<int> hist = w->getImageHistogram();
		uint min, max;
		double newIntensity;
		for (int i = 0; i < hist.size(); i++)
			if (hist[i] > 0)
				max = i;
		for (int i = max; i >= 0; i--)
			if (hist[i] > 0)
				min = i;
		msgBox.setText("Min: " + QString::number(min) + "  Max:  " + QString::number(max));
		msgBox.exec();
		for (int i = 0; i < w->getImgHeight(); i++)
			for (int j = 0; j < w->getImgWidth(); j++){
				newIntensity = 255.0*((data[i * row + j] - min) / (double)(max - min));
				w->setPixel(w->getImage(), j, i, static_cast<uchar>(round(newIntensity)));
			}
	}
	else {
		w->calcHistogramRGB();
		QVector<int> histR = w->getImageHistogramR();
		QVector<int> histG = w->getImageHistogramG();
		QVector<int> histB = w->getImageHistogramB();
		uint minR, maxR, minG, maxG, minB, maxB;
		double newIntensityR, newIntensityG, newIntensityB;
		for (int i = 0; i < histR.size(); i++) {
			if (histR[i] > 0)
				maxR = i;
			if (histG[i] > 0)
				maxG = i;
			if (histB[i] > 0)
				maxB = i;
		}
		for (int i = 255; i >= 0; i--) {
			if (histR[i] > 0)
				minR = i;
			if (histG[i] > 0)
				minG = i;
			if (histB[i] > 0)
				minB = i;
		}
		msgBox.setText(" MinR: " + QString::number(minR) + "  MaxR:  " + QString::number(maxR) + " \n MinG: " + QString::number(minG) + "  MaxG:  " + QString::number(maxG) + " \n MinB: " + QString::number(minB) + "  MaxB:  " + QString::number(maxB));
		msgBox.exec();
		for (int i = 0; i < w->getImgHeight(); i++)
			for (int j = 0; j < w->getImgWidth(); j++) {
				newIntensityR = 255.0*((data[i * row + j * 4] - minR) / (double)(maxR - minR));
				newIntensityG = 255.0*((data[i * row + j * 4 + 1] - minG) / (double)(maxG - minG));
				newIntensityB = 255.0*((data[i * row + j * 4 + 2] - minB) / (double)(maxB - minB));
				w->setPixel(w->getImage(), j, i, static_cast<uchar>(round(newIntensityR)), static_cast<uchar>(round(newIntensityG)), static_cast<uchar>(round(newIntensityB)));
			}
	}

	w->update();
	return true;
}
bool ImageViewer::convolution(int size, QList<double> mask)
{
	int k = (size - 1) / 2;
	QVector<int> mirrored = mirroring(k);
	ViewerWidget* w = getCurrentViewerWidget();
	int depth = w->getImage()->depth();
	int rowNew, row = w->getImage()->bytesPerLine();
	if (depth == 8) {
		if (k % 2 == 0)
			rowNew = row + k * 2;
		else
			rowNew = row + (k + 1) * 2;
	}
	else {
		if (k % 2 == 0)
			rowNew = row + k * 2 * 3;
		else
			rowNew = row + (k + 1) * 2 * 3;
	}
	double sumMask = 0;
	for (int iMask = 0; iMask < size; iMask++)
		for (int jMask = 0; jMask < size; jMask++)
			sumMask += mask[iMask * size + jMask];
	if (depth == 8) {
		double newIntensity;
		for (int i = k; i < k + w->getImgHeight(); i++) {
			for (int j = k; j < k + w->getImgWidth(); j++) {
				newIntensity = 0;
				for (int iMask = 0; iMask < size; iMask++)
					for (int jMask = 0; jMask < size; jMask++)
						newIntensity += (mask[iMask * size + jMask] / sumMask) * mirrored[(i - k + iMask) * rowNew + (j - k + jMask)];
				w->setPixel(w->getImage(), j - k, i - k, static_cast<uchar>(round(newIntensity)));
			}
		}
	}
	else {
		double newIntensityR, newIntensityG, newIntensityB;
		for (int i = k; i < k + w->getImgHeight(); i++) {
			for (int j = k; j < k + w->getImgWidth(); j++) {
				newIntensityR = 0;
				newIntensityG = 0;
				newIntensityB = 0;
				for (int iMask = 0; iMask < size; iMask++)
					for (int jMask = 0; jMask < size; jMask++) {
						newIntensityR += (mask[iMask * size + jMask] / sumMask) * mirrored[(i - k + iMask) * rowNew + (j - k + jMask) * 3];
						newIntensityG += (mask[iMask * size + jMask] / sumMask) * mirrored[(i - k + iMask) * rowNew + (j - k + jMask) * 3 + 1];
						newIntensityB += (mask[iMask * size + jMask] / sumMask) * mirrored[(i - k + iMask) * rowNew + (j - k + jMask) * 3 + 2];
					}
				w->setPixel(w->getImage(), j - k, i - k, static_cast<uchar>(round(newIntensityR)), static_cast<uchar>(round(newIntensityG)), static_cast<uchar>(round(newIntensityB)));
			}
		}
	}
	
	w->update();
	return true;
}
bool ImageViewer::BHThreshold()
{	
	ViewerWidget* w = getCurrentViewerWidget();
	uchar* data = w->getData();
	w->calcHistogramBW();
	QVector<int> hist = w->getImageHistogram();
	int row = w->getImage()->bytesPerLine();
	int depth = w->getImage()->depth();
	uint weightL = 0, weightR = 0;
	int iStart = 0, iEnd = 255, min_count = round((w->getImgHeight() *  w->getImgWidth()) / row);
	while (hist[iStart] <= min_count)
		iStart ++;
	while (hist[iEnd] <= min_count)
		iEnd --;
	int T = int(round((iEnd + iStart) / 2.0));
	int new_T = T;

	if (depth == 8) {
		for (int i = iStart; i < T + 1; i++)
			weightL += hist[i];
		for (int i = T + 1; i < iEnd + 1; i++)
			weightR += hist[i];
		while (iStart < iEnd) {
			if (weightL > weightR) {						//left part heavier
				weightL -= hist[iStart];
				iStart++;
			}
			else {											//right part heavier	
				weightR -= hist[iEnd];
				iEnd--;
			}
			new_T = int(round((iEnd + iStart) / 2.0));		//re - center the weighing scale
			if (new_T < T) {
				weightL -= hist[T];
				weightR += hist[T];
			}
			else if (new_T > T) {
				weightL += hist[T];
				weightR -= hist[T];
			}
			T = new_T;
		}
		msgBox.setText("Threshold: " + QString::number(T));
		msgBox.exec();
		for (int i = 0; i < w->getImgHeight(); i++)
			for (int j = 0; j < w->getImgWidth(); j++) {
				if (data[i * row + j] >= T)
					w->setPixel(w->getImage(), j, i, static_cast<uchar>(255));
				else
					w->setPixel(w->getImage(), j, i, static_cast<uchar>(0));
			}
	}
	else {
		msgBox.setText("RGB picture.....not done yet");					//HOW?
		msgBox.exec();
	}

	w->update();
	return true;
}
bool ImageViewer::OtsuThreshold()
{
	ViewerWidget* w = getCurrentViewerWidget();
	uchar* data = w->getData();
	w->calcHistogramBW();
	QVector<int> hist = w->getImageHistogram();
	int row = w->getImage()->bytesPerLine();
	int depth = w->getImage()->depth();
	uint N = w->getImgWidth() * w->getImgHeight();
	int T, sum, sumB, wB, wF;
	double var_max, sigma, muF, muB;
	sum = 0;
	sumB = 0;
	wB = 0;
	var_max = 0.0;

	if (depth == 8) {
		for (int i = 0; i <= 255; i++)
			sum += i * hist[i];
		for (int t = 0; t < 256; t++) {
			wF = N - wB;
			if (wB > 0 && wF > 0) {
				muB = (sumB / (double)wB);
				muF = (sum - sumB) / (double)wF;
				sigma = wB * wF * (muB - muF) * (muB - muF);		//rozptyl
				if (sigma >= var_max) {
					T = t;
					var_max = sigma;
				}
			}
			wB += hist[t];
			sumB += t * hist[t];
		}
		for (int i = 0; i < w->getImgHeight(); i++)
			for (int j = 0; j < w->getImgWidth(); j++) {
				if (data[i * row + j] >= T)
					w->setPixel(w->getImage(), j, i, static_cast<uchar>(255));
				else
					w->setPixel(w->getImage(), j, i, static_cast<uchar>(0));
			}
	}
	else {
		msgBox.setText("RGB picture.....not done yet");
		msgBox.exec();
	}

	w->update();
	return true;
}
bool ImageViewer::adaptiveThreshold()
{
	int k = 3;
	int size = 2 * k + 1;
	double koef = 1.0 / (double)(size * size);
	int T = 10;
	QVector<int> mirrored = mirroring(k);
	ViewerWidget* w = getCurrentViewerWidget();
	uchar* data = w->getData();
	int depth = w->getImage()->depth();
	int rowNew, row = w->getImage()->bytesPerLine();
	if (k % 2 == 0)
		rowNew = row + k * 2;
	else
		rowNew = row + (k + 1) * 2;

	if (depth == 8) {
		double newIntensity, diff;
		for (int i = k; i < k + w->getImgHeight(); i++) {
			for (int j = k; j < k + w->getImgWidth(); j++) {
				newIntensity = 0;
				for (int iMask = 0; iMask < size; iMask++)
					for (int jMask = 0; jMask < size; jMask++) {
						newIntensity += koef * mirrored[(i - k + iMask) * rowNew + (j - k + jMask)];
					}
				diff = abs(newIntensity - data[(i - k) * row + (j - k)]);
				//msgBox.setText("new intensity: " + QString::number(newIntensity, 'f') + "  diff: " + QString::number(diff, 'f'));
				//msgBox.exec();
				if (abs(diff) >= T)
					w->setPixel(w->getImage(), j - k, i - k, static_cast<uchar>(0));
				else
					w->setPixel(w->getImage(), j - k, i - k, static_cast<uchar>(255));
			}
		}
	}
	else {
		msgBox.setText("RGB picture.....not done yet");
		msgBox.exec();
	}

	w->update();
	return true;
}

//Slots

//Tabs slots
void ImageViewer::on_tabWidget_tabCloseRequested(int tabId)
{
	ViewerWidget* vW = getViewerWidget(tabId);
	vW->~ViewerWidget();
	ui->tabWidget->removeTab(tabId);
	tabCount--;
}
void ImageViewer::on_actionRename_triggered()
{
	if (!isImgOpened()) {
		msgBox.setText("No image is opened.");
		msgBox.setIcon(QMessageBox::Information);
		msgBox.exec();
		return;
	}
	ViewerWidget* w = getCurrentViewerWidget();
	bool ok;
	QString text = QInputDialog::getText(this, QString("Rename"), tr("Image name:"), QLineEdit::Normal, w->getName(), &ok);
	if (ok && !text.trimmed().isEmpty())
	{
		w->setName(text);
		ui->tabWidget->setTabText(ui->tabWidget->currentIndex(), text);
	}
}
void ImageViewer::tab_changed(int i)
{
	if (ui->tabWidget->count() > tabCount)
		return;
	else {
		ViewerWidget* w = getCurrentViewerWidget();
		if (w->getImgListSize() != 0) {
			ui->horizontalSlider->setValue(0);
			ui->horizontalSlider->setVisible(true);
		}
		else
			ui->horizontalSlider->setHidden(true);
	}
}

//Image slots
void ImageViewer::on_actionNew_triggered()
{
	newImgDialog = new NewImageDialog(this);
	connect(newImgDialog, SIGNAL(accepted()), this, SLOT(newImageAccepted()));
	newImgDialog->exec();
}
void ImageViewer::newImageAccepted()
{
	NewImageDialog* newImgDialog = static_cast<NewImageDialog*>(sender());

	int width = newImgDialog->getWidth();
	int height = newImgDialog->getHeight();
	QString name = newImgDialog->getName();

	openNewTabForImg(new ViewerWidget(name, QSize(width, height)));
	ui->tabWidget->setCurrentIndex(ui->tabWidget->count() - 1);

	newImgDialog->deleteLater();
}
void ImageViewer::on_actionOpen_triggered()
{
	QString folder = settings.value("folder_img_load_path", "").toString();

	QString fileFilter = "Image data (*.bmp *.gif *.jpg *.jpeg *.png *.pbm *.pgm *.ppm .*xbm .* xpm);;All files (*)";
	QString fileName = QFileDialog::getOpenFileName(this, "Load image", folder, fileFilter);
	if (fileName.isEmpty()) { return; }

	QFileInfo fi(fileName);
	settings.setValue("folder_img_load_path", fi.absoluteDir().absolutePath());

	if (!openImage(fileName)) {
		msgBox.setText("Unable to open image.");
		msgBox.setIcon(QMessageBox::Warning);
		msgBox.exec();
	}
	ui->horizontalSlider->setHidden(true);
}
void ImageViewer::on_actionSave_as_triggered()
{
	if (!isImgOpened()) {
		msgBox.setText("No image to save.");
		msgBox.setIcon(QMessageBox::Information);
		msgBox.exec();
		return;
	}
	QString folder = settings.value("folder_img_save_path", "").toString();

	ViewerWidget* w = getCurrentViewerWidget();

	QString fileFilter = "Image data (*.bmp *.gif *.jpg *.jpeg *.png *.pbm *.pgm *.ppm .*xbm .* xpm);;All files (*)";
	QString fileName = QFileDialog::getSaveFileName(this, "Save image", folder + "/" + w->getName(), fileFilter);
	if (fileName.isEmpty()) { return; }

	QFileInfo fi(fileName);
	settings.setValue("folder_img_save_path", fi.absoluteDir().absolutePath());

	if (!saveImage(fileName)) {
		msgBox.setText("Unable to save image.");
		msgBox.setIcon(QMessageBox::Warning);
		msgBox.exec();
	}
	else {
		msgBox.setText(QString("File %1 saved.").arg(fileName));
		msgBox.setIcon(QMessageBox::Information);
		msgBox.exec();
	}
}
void ImageViewer::on_actionClear_triggered()
{
	if (!isImgOpened()) {
		msgBox.setText("No image is opened.");
		msgBox.setIcon(QMessageBox::Information);
		msgBox.exec();
		return;
	}
	clearImage();
}
void ImageViewer::on_actionInvert_colors_triggered() {
	if (!isImgOpened()) {
		msgBox.setText("No image is opened.");
		msgBox.setIcon(QMessageBox::Information);
		msgBox.exec();
		return;
	}
	ui->loadingLabel->setVisible(true);
	qApp->processEvents();
	invertColors();
	ui->loadingLabel->setHidden(true);
}
void ImageViewer::FSHS_clicked()
{
	if (!isImgOpened()) {
		msgBox.setText("No image is opened.");
		msgBox.setIcon(QMessageBox::Information);
		msgBox.exec();
		return;
	}
	ui->loadingLabel->setVisible(true);
	qApp->processEvents();
	FSHS();
	ui->loadingLabel->setHidden(true);
}
void ImageViewer::convolution_clicked()
{
	if (!isImgOpened()) {
		msgBox.setText("No image is opened.");
		msgBox.setIcon(QMessageBox::Information);
		msgBox.exec();
		return;
	}
	
	QString folder = settings.value("folder_img_load_path", "").toString();
	QString fileMask = "Image data (*.txt)";
	QString fileName = QFileDialog::getOpenFileName(this, "Load image", folder, fileMask);
	if (fileName.isEmpty()) { return; }
	ui->loadingLabel->setVisible(true);
	qApp->processEvents();

	QFileInfo fi(fileName);
	settings.setValue("folder_img_load_path", fi.absoluteDir().absolutePath());
	QFile file(fileName);
	if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
		return;

	QTextStream stream(&file);
	QString line;
	QStringList list;
	QList<double> mask;
	while (!stream.atEnd()) {
		line = stream.readLine();
		list = line.split(" ");
		for (int i = 0; i < list.size(); i++)
			mask << list[i].toDouble();
	}
	int n = list.size();
	convolution(n, mask);
	ui->loadingLabel->setHidden(true);
}
void ImageViewer::histThresholding_clicked()
{
	if (!isImgOpened()) {
		msgBox.setText("No image is opened.");
		msgBox.setIcon(QMessageBox::Information);
		msgBox.exec();
		return;
	}
	ui->loadingLabel->setVisible(true);
	qApp->processEvents();
	BHThreshold();
	ui->loadingLabel->setHidden(true);
}
void ImageViewer::otsuThresholding_clicked()
{
	if (!isImgOpened()) {
		msgBox.setText("No image is opened.");
		msgBox.setIcon(QMessageBox::Information);
		msgBox.exec();
		return;
	}
	ui->loadingLabel->setVisible(true);
	qApp->processEvents();
	OtsuThreshold();
	ui->loadingLabel->setHidden(true);
}
void ImageViewer::adaptThresholding_clicked()
{
	if (!isImgOpened()) {
		msgBox.setText("No image is opened.");
		msgBox.setIcon(QMessageBox::Information);
		msgBox.exec();
		return;
	}
	ui->loadingLabel->setVisible(true);
	qApp->processEvents();
	adaptiveThreshold();
	ui->loadingLabel->setHidden(true);
}
void ImageViewer::mirrorHalf_clicked()
{
	if (!isImgOpened()) {
		msgBox.setText("No image is opened.");
		msgBox.setIcon(QMessageBox::Information);
		msgBox.exec();
		return;
	}
	ui->loadingLabel->setVisible(true);
	qApp->processEvents();
	ViewerWidget* w = getCurrentViewerWidget();
	int k;
	if (round(w->getImgWidth() / 2.0) < w->getImgHeight())
		k = round(w->getImgWidth() / 2.0);
	else
		k = round(w->getImgHeight() / 2.0);
	QVector<int> mirrored = mirroring(k);
	int depth = w->getImage()->depth();
	int rowNew, row = w->getImage()->bytesPerLine();
	if (depth == 8) {
		if (k % 2 == 0)
			rowNew = row + k * 2;
		else
			rowNew = row + (k + 1) * 2;
	}
	else {
		if (k % 2 == 0)
			rowNew = row + k * 2 * 3;
		else
			rowNew = row + (k + 1) * 2 * 3;
	}
	w->resizeImg(k * 2 + w->getImgWidth(), k * 2 + w->getImgHeight());	

	if (depth == 8)
		for (int i = 0; i < w->getImgHeight(); i++)
			for (int j = 0; j < w->getImgWidth(); j++)
				w->setPixel(w->getImage(), j, i, static_cast<uchar>(mirrored[i * rowNew + j]));
	else {
		int r, g, b;
		for (int i = 0; i < w->getImgHeight(); i++)
			for (int j = 0; j < w->getImgWidth(); j++) {
				r = mirrored[i * rowNew + j * 3];
				g = mirrored[i * rowNew + j * 3 + 1] ;
				b=  mirrored[i * rowNew + j * 3 + 2];
				w->setPixel(w->getImage(), j, i, static_cast<uchar>(r), static_cast<uchar>(g), static_cast<uchar>(b));
			}
	}
	w->setDataPtr();
	w->update();
	ui->loadingLabel->setHidden(true);
}
void ImageViewer::mirrorFull_clicked()
{
	if (!isImgOpened()) {
		msgBox.setText("No image is opened.");
		msgBox.setIcon(QMessageBox::Information);
		msgBox.exec();
		return;
	}
	ui->loadingLabel->setVisible(true);
	qApp->processEvents();
	ViewerWidget* w = getCurrentViewerWidget();
	int k;
	if (w->getImgWidth() < w->getImgHeight())
		k = w->getImgWidth();
	else
		k = w->getImgHeight();
	QVector<int> mirrored = mirroring(k);
	int depth = w->getImage()->depth();
	int rowNew, row = w->getImage()->bytesPerLine();;
	if (depth == 8) {
		if (k % 2 == 0)
			rowNew = row + k * 2;
		else
			rowNew = row + (k + 1) * 2;
	}
	else {
		if (k % 2 == 0)
			rowNew = row + k * 2 * 3;
		else
			rowNew = row + (k + 1) * 2 * 3;
	}
	w->resizeImg(k * 2 + w->getImgWidth(), k * 2 + w->getImgHeight());

	if (depth == 8)
		for (int i = 0; i < w->getImgHeight(); i++)
			for (int j = 0; j < w->getImgWidth(); j++)
				w->setPixel(w->getImage(), j, i, static_cast<uchar>(mirrored[i * rowNew + j]));
	else {
		int r, g, b;
		for (int i = 0; i < w->getImgHeight(); i++)
			for (int j = 0; j < w->getImgWidth(); j++) {
				r = mirrored[i * rowNew + j * 3];
				g = mirrored[i * rowNew + j * 3 + 1];
				b = mirrored[i * rowNew + j * 3 + 2];
				w->setPixel(w->getImage(), j, i, static_cast<uchar>(r), static_cast<uchar>(g), static_cast<uchar>(b));
			}
	}
	w->setDataPtr();
	w->update();
	ui->loadingLabel->setHidden(true);
}
void ImageViewer::mirrorQuarter_clicked()
{
	if (!isImgOpened()) {
		msgBox.setText("No image is opened.");
		msgBox.setIcon(QMessageBox::Information);
		msgBox.exec();
		return;
	}
	ui->loadingLabel->setVisible(true);
	qApp->processEvents();
	ViewerWidget* w = getCurrentViewerWidget();
	int k;
	if (round(w->getImgWidth() / 4.0) < w->getImgHeight())
		k = round(w->getImgWidth() / 4.0);
	else
		k = round(w->getImgHeight() / 4.0);
	QVector<int> mirrored = mirroring(k);
	int depth = w->getImage()->depth();
	int rowNew, row = w->getImage()->bytesPerLine();;
	if (depth == 8) {
		if (k % 2 == 0)
			rowNew = row + k * 2;
		else
			rowNew = row + (k + 1) * 2;
	}
	else {
		if (k % 2 == 0)
			rowNew = row + k * 2 * 3;
		else
			rowNew = row + (k + 1) * 2 * 3;
	}
	w->resizeImg(k * 2 + w->getImgWidth(), k * 2 + w->getImgHeight());

	if (depth == 8)
		for (int i = 0; i < w->getImgHeight(); i++)
			for (int j = 0; j < w->getImgWidth(); j++)
				w->setPixel(w->getImage(), j, i, static_cast<uchar>(mirrored[i * rowNew + j]));
	else {
		int r, g, b;
		for (int i = 0; i < w->getImgHeight(); i++)
			for (int j = 0; j < w->getImgWidth(); j++) {
				r = mirrored[i * rowNew + j * 3];
				g = mirrored[i * rowNew + j * 3 + 1];
				b = mirrored[i * rowNew + j * 3 + 2];
				w->setPixel(w->getImage(), j, i, static_cast<uchar>(r), static_cast<uchar>(g), static_cast<uchar>(b));
			}
	}
	w->setDataPtr();
	w->update();
	ui->loadingLabel->setHidden(true);
}
void ImageViewer::explicitLinDif_clicked()
{
	if (!isImgOpened()) {
		msgBox.setText("No image is opened.");
		msgBox.setIcon(QMessageBox::Information);
		msgBox.exec();
		return;
	}
	if (ui->nTauLinDifSpinBox->value() < ui->nSaveLinDifSpinBox->value()) {
		msgBox.setText("Number of iterations is too small");
		msgBox.setIcon(QMessageBox::Information);
		msgBox.exec();
		ui->nTauLinDifSpinBox->setValue(ui->nSaveLinDifSpinBox->value());
	}
	ui->loadingLabel->setText("LOADING STEP:   " + QString::number(1));
	ui->loadingLabel->setVisible(true);
	ui->horizontalSlider->setVisible(true);
	qApp->processEvents();

	ViewerWidget* w = getCurrentViewerWidget();
	uchar* data = w->getData();
	Filters f;
	int k = 1;
	int depth = w->getImage()->depth();
	int rowNew, row = w->getImage()->bytesPerLine();
	if (depth == 8)
		rowNew = row + (k + 1) * 2;
	else
		rowNew = row + (k + 1) * 2 * 3;

	w->calcHistogramBW();
	std::cout << w->getMeanBW() << "\n";

	w->clearImgList();
	QVector<int> U0 = mirroring(k), Un;
	int nSteps = round(ui->nTauLinDifSpinBox->value() / ui->nSaveLinDifSpinBox->value());
	
	for (int step = 0; step < nSteps; step++) {
		Un = f.explicitLinDif(U0, ui->tauLinDifSpinBox->value(), ui->nSaveLinDifSpinBox->value(), w->getImgWidth(), w->getImgHeight(), rowNew, false);
		if (depth == 8)
			for (int i = k; i < k + w->getImgHeight(); i++)
				for (int j = k; j < k + w->getImgWidth(); j++)
					w->setPixel(w->getImage(), j - k, i - k, static_cast<uchar>(Un[i * rowNew + j]));
		else {
			Un = f.explicitLinDif(U0, ui->tauLinDifSpinBox->value(), ui->nSaveLinDifSpinBox->value(), w->getImgWidth(), w->getImgHeight(), rowNew, true);
			for (int i = k; i < k + w->getImgHeight(); i++)
				for (int j = k; j < k + w->getImgWidth(); j++) {
					w->setPixel(w->getImage(), j - k, i - k, static_cast<uchar>(Un[i * rowNew + j * 3]), static_cast<uchar>(Un[i * rowNew + j * 3 + 1]), static_cast<uchar>(Un[i * rowNew + j * 3 + 2]));
				}
		}
		w->saveIteration();
		U0 = Un;
		w->update();
		w->calcHistogramBW();
		std::cout << w->getMeanBW() << "\n";
		ui->loadingLabel->setText("LOADING STEP:   " + QString::number(step + 2));
		qApp->processEvents();
	}
	int mod = ui->nTauLinDifSpinBox->value() % ui->nSaveLinDifSpinBox->value();
	if (mod != 0) {
		Un = f.explicitLinDif(U0, ui->tauLinDifSpinBox->value(), mod, w->getImgWidth(), w->getImgHeight(), rowNew, false);
		if (depth == 8)
			for (int i = k; i < k + w->getImgHeight(); i++)
				for (int j = k; j < k + w->getImgWidth(); j++)
					w->setPixel(w->getImage(), j - k, i - k, static_cast<uchar>(Un[i * rowNew + j]));
		else {
			Un = f.explicitLinDif(U0, ui->tauLinDifSpinBox->value(), mod, w->getImgWidth(), w->getImgHeight(), rowNew, true);
			for (int i = k; i < k + w->getImgHeight(); i++)
				for (int j = k; j < k + w->getImgWidth(); j++)
					w->setPixel(w->getImage(), j - k, i - k, static_cast<uchar>(Un[i * rowNew + j * 3]), static_cast<uchar>(Un[i * rowNew + j * 3 + 1]), static_cast<uchar>(Un[i * rowNew + j * 3 + 2]));
		}
		w->saveIteration();
		w->update();
		w->calcHistogramBW();
		std::cout << w->getMeanBW() << "\n";
	}

	ui->horizontalSlider->setMaximum(w->getImgListSize() - 1);
	ui->horizontalSlider->setValue(ui->horizontalSlider->maximum());
	ui->loadingLabel->setHidden(true);
	ui->loadingLabel->setText("LOADING...");
}
void ImageViewer::implicitLinDif_clicked()
{
	if (!isImgOpened()) {
		msgBox.setText("No image is opened.");
		msgBox.setIcon(QMessageBox::Information);
		msgBox.exec();
		return;
	}
	if (ui->nTauLinDifSpinBox->value() < ui->nSaveLinDifSpinBox->value()) {
		msgBox.setText("Number of iterations is too small");
		msgBox.setIcon(QMessageBox::Information);
		msgBox.exec();
		ui->nTauLinDifSpinBox->setValue(ui->nSaveLinDifSpinBox->value());
	}
	ui->loadingLabel->setText("LOADING STEP:   " + QString::number(1));
	ui->loadingLabel->setVisible(true);
	ui->horizontalSlider->setVisible(true);
	qApp->processEvents();

	ViewerWidget* w = getCurrentViewerWidget();
	uchar* data = w->getData();
	Filters f;
	int k = 1;
	int depth = w->getImage()->depth();
	int rowNew, row = w->getImage()->bytesPerLine();
	if (depth == 8)
		rowNew = row + (k + 1) * 2;
	else
		rowNew = row + (k + 1) * 2 * 3;

	w->calcHistogramBW();
	std::cout << w->getMeanBW() << "\n";
	
	w->clearImgList();
	QVector<int> U0 = mirroring(k), Un;
	int nSteps = round(ui->nTauLinDifSpinBox->value() / ui->nSaveLinDifSpinBox->value());
	
	for (int step = 0; step < nSteps; step++) {
		Un = f.implicitLinDif(U0, ui->tauLinDifSpinBox->value(), ui->nSaveLinDifSpinBox->value(), w->getImgWidth(), w->getImgHeight(), rowNew, false);
		if (depth == 8)
			for (int i = k; i < k + w->getImgHeight(); i++)
				for (int j = k; j < k + w->getImgWidth(); j++)
					w->setPixel(w->getImage(), j - k, i - k, static_cast<uchar>(Un[i * rowNew + j]));
		else {
//			Un = f.implicitLinDif(U0, ui->tauLinDifSpinBox->value(), ui->nSaveLinDifSpinBox->value(), w->getImgWidth(), w->getImgHeight(), rowNew, true);
//			for (int i = k; i < k + w->getImgHeight(); i++)
//				for (int j = k; j < k + w->getImgWidth(); j++) {
//					w->setPixel(w->getImage(), j - k, i - k, static_cast<uchar>(Un[i * rowNew + j * 3]), static_cast<uchar>(Un[i * rowNew + j * 3 + 1]), static_cast<uchar>(Un[i * rowNew + j * 3 + 2]));
//				}
		}
		w->saveIteration();
		U0 = Un;
		w->update();
		w->calcHistogramBW();
		std::cout << w->getMeanBW() << "\n";
		ui->loadingLabel->setText("LOADING STEP:   " + QString::number(step + 2));
		qApp->processEvents();
	}
	int mod = ui->nTauLinDifSpinBox->value() % ui->nSaveLinDifSpinBox->value();
	if (mod != 0) {
		Un = f.implicitLinDif(U0, ui->tauLinDifSpinBox->value(), mod, w->getImgWidth(), w->getImgHeight(), rowNew, false);
		if (depth == 8)
			for (int i = k; i < k + w->getImgHeight(); i++)
				for (int j = k; j < k + w->getImgWidth(); j++)
					w->setPixel(w->getImage(), j - k, i - k, static_cast<uchar>(Un[i * rowNew + j]));
		else {
//			Un = f.implicitLinDif(U0, ui->tauLinDifSpinBox->value(), mod, w->getImgWidth(), w->getImgHeight(), rowNew, true);
//			for (int i = k; i < k + w->getImgHeight(); i++)
//				for (int j = k; j < k + w->getImgWidth(); j++)
//					w->setPixel(w->getImage(), j - k, i - k, static_cast<uchar>(Un[i * rowNew + j * 3]), static_cast<uchar>(Un[i * rowNew + j * 3 + 1]), static_cast<uchar>(Un[i * rowNew + j * 3 + 2]));
		}
		w->saveIteration();
		w->update();
		w->calcHistogramBW();
		std::cout << w->getMeanBW() << "\n";
	}

	ui->horizontalSlider->setMaximum(w->getImgListSize() - 1);
	ui->horizontalSlider->setValue(ui->horizontalSlider->maximum());
	ui->loadingLabel->setHidden(true);
	ui->loadingLabel->setText("LOADING...");

}
void ImageViewer::explicitPM_clicked()
{
	if (!isImgOpened()) {
		msgBox.setText("No image is opened.");
		msgBox.setIcon(QMessageBox::Information);
		msgBox.exec();
		return;
	}
	if (ui->nTauPMSpinBox->value() < ui->nSavePMSpinBox->value()) {
		msgBox.setText("Number of iterations is too small");
		msgBox.setIcon(QMessageBox::Information);
		msgBox.exec();
		ui->nTauPMSpinBox->setValue(ui->nSavePMSpinBox->value());
	}
	ui->loadingLabel->setText("LOADING STEP:   " + QString::number(1));
	ui->loadingLabel->setVisible(true);
	ui->horizontalSlider->setVisible(true);
	qApp->processEvents();

	ViewerWidget* w = getCurrentViewerWidget();
	uchar* data = w->getData();
	Filters f;
	int k = 1;
	int depth = w->getImage()->depth();
	int rowNew, row = w->getImage()->bytesPerLine();
	if (depth == 8)
		rowNew = row + (k + 1) * 2;
	else
		rowNew = row + (k + 1) * 2 * 3;

	w->calcHistogramBW();
	std::cout << w->getMeanBW() << "\n";

	w->clearImgList();
	QVector<int> U0 = mirroring(k), Un;
	int nSteps = round(ui->nTauPMSpinBox->value() / ui->nSavePMSpinBox->value());

	for (int step = 0; step < nSteps; step++) {
		Un = f.explicitPM(U0, ui->tauPMSpinBox->value(), ui->nSavePMSpinBox->value(), w->getImgWidth(), w->getImgHeight(), rowNew, false);
		if (depth == 8)
			for (int i = k; i < k + w->getImgHeight(); i++)
				for (int j = k; j < k + w->getImgWidth(); j++)
					w->setPixel(w->getImage(), j - k, i - k, static_cast<uchar>(Un[i * rowNew + j]));
		else {
			Un = f.explicitPM(U0, ui->tauPMSpinBox->value(), ui->nSavePMSpinBox->value(), w->getImgWidth(), w->getImgHeight(), rowNew, true);
			for (int i = k; i < k + w->getImgHeight(); i++)
				for (int j = k; j < k + w->getImgWidth(); j++) {
					w->setPixel(w->getImage(), j - k, i - k, static_cast<uchar>(Un[i * rowNew + j * 3]), static_cast<uchar>(Un[i * rowNew + j * 3 + 1]), static_cast<uchar>(Un[i * rowNew + j * 3 + 2]));
				}
		}
		w->saveIteration();
		U0 = Un;
		w->update();
		w->calcHistogramBW();
		std::cout << w->getMeanBW() << "\n";
		ui->loadingLabel->setText("LOADING STEP:   " + QString::number(step + 2));
		qApp->processEvents();
	}
	int mod = ui->nTauPMSpinBox->value() % ui->nSavePMSpinBox->value();
	if (mod != 0) {
		Un = f.explicitPM(U0, ui->tauPMSpinBox->value(), mod, w->getImgWidth(), w->getImgHeight(), rowNew, false);
		if (depth == 8)
			for (int i = k; i < k + w->getImgHeight(); i++)
				for (int j = k; j < k + w->getImgWidth(); j++)
					w->setPixel(w->getImage(), j - k, i - k, static_cast<uchar>(Un[i * rowNew + j]));
		else {
			Un = f.explicitPM(U0, ui->tauPMSpinBox->value(), mod, w->getImgWidth(), w->getImgHeight(), rowNew, true);
			for (int i = k; i < k + w->getImgHeight(); i++)
				for (int j = k; j < k + w->getImgWidth(); j++)
					w->setPixel(w->getImage(), j - k, i - k, static_cast<uchar>(Un[i * rowNew + j * 3]), static_cast<uchar>(Un[i * rowNew + j * 3 + 1]), static_cast<uchar>(Un[i * rowNew + j * 3 + 2]));
		}
		w->saveIteration();
		w->update();
		w->calcHistogramBW();
		std::cout << w->getMeanBW() << "\n";
	}

	ui->horizontalSlider->setMaximum(w->getImgListSize() - 1);
	ui->horizontalSlider->setValue(ui->horizontalSlider->maximum());
	ui->loadingLabel->setHidden(true);
	ui->loadingLabel->setText("LOADING...");
}
void ImageViewer::semiImplicitPM_clicked()
{
	if (!isImgOpened()) {
		msgBox.setText("No image is opened.");
		msgBox.setIcon(QMessageBox::Information);
		msgBox.exec();
		return;
	}
	if (ui->nTauPMSpinBox->value() < ui->nSavePMSpinBox->value()) {
		msgBox.setText("Number of iterations is too small");
		msgBox.setIcon(QMessageBox::Information);
		msgBox.exec();
		ui->nTauPMSpinBox->setValue(ui->nSavePMSpinBox->value());
	}
	ui->loadingLabel->setText("LOADING STEP:   " + QString::number(1));
	ui->loadingLabel->setVisible(true);
	ui->horizontalSlider->setVisible(true);
	qApp->processEvents();

	ViewerWidget* w = getCurrentViewerWidget();
	uchar* data = w->getData();
	Filters f;
	int k = 1;
	int depth = w->getImage()->depth();
	int rowNew, row = w->getImage()->bytesPerLine();
	if (depth == 8)
		rowNew = row + (k + 1) * 2;
	else
		rowNew = row + (k + 1) * 2 * 3;

	w->calcHistogramBW();
	std::cout << w->getMeanBW() << "\n";

	w->clearImgList();
	QVector<int> U0 = mirroring(k), Un;
	int nSteps = round(ui->nTauPMSpinBox->value() / ui->nSavePMSpinBox->value());

	for (int step = 0; step < nSteps; step++) {
		Un = f.semiImplicitPM(U0, ui->tauPMSpinBox->value(), ui->nSavePMSpinBox->value(), w->getImgWidth(), w->getImgHeight(), rowNew, false);
		if (depth == 8)
			for (int i = k; i < k + w->getImgHeight(); i++)
				for (int j = k; j < k + w->getImgWidth(); j++)
					w->setPixel(w->getImage(), j - k, i - k, static_cast<uchar>(Un[i * rowNew + j]));
		else {
			Un = f.semiImplicitPM(U0, ui->tauPMSpinBox->value(), ui->nSavePMSpinBox->value(), w->getImgWidth(), w->getImgHeight(), rowNew, true);
			for (int i = k; i < k + w->getImgHeight(); i++)
				for (int j = k; j < k + w->getImgWidth(); j++) {
					w->setPixel(w->getImage(), j - k, i - k, static_cast<uchar>(Un[i * rowNew + j * 3]), static_cast<uchar>(Un[i * rowNew + j * 3 + 1]), static_cast<uchar>(Un[i * rowNew + j * 3 + 2]));
				}
		}
		w->saveIteration();
		U0 = Un;
		w->update();
		w->calcHistogramBW();
		std::cout << w->getMeanBW() << "\n";
		ui->loadingLabel->setText("LOADING STEP:   " + QString::number(step + 2));
		qApp->processEvents();
	}
	int mod = ui->nTauPMSpinBox->value() % ui->nSavePMSpinBox->value();
	if (mod != 0) {
		Un = f.semiImplicitPM(U0, ui->tauPMSpinBox->value(), mod, w->getImgWidth(), w->getImgHeight(), rowNew, false);
		if (depth == 8)
			for (int i = k; i < k + w->getImgHeight(); i++)
				for (int j = k; j < k + w->getImgWidth(); j++)
					w->setPixel(w->getImage(), j - k, i - k, static_cast<uchar>(Un[i * rowNew + j]));
		else {
			Un = f.semiImplicitPM(U0, ui->tauPMSpinBox->value(), mod, w->getImgWidth(), w->getImgHeight(), rowNew, true);
			for (int i = k; i < k + w->getImgHeight(); i++)
				for (int j = k; j < k + w->getImgWidth(); j++)
					w->setPixel(w->getImage(), j - k, i - k, static_cast<uchar>(Un[i * rowNew + j * 3]), static_cast<uchar>(Un[i * rowNew + j * 3 + 1]), static_cast<uchar>(Un[i * rowNew + j * 3 + 2]));
		}
		w->saveIteration();
		w->update();
		w->calcHistogramBW();
		std::cout << w->getMeanBW() << "\n";
	}

	ui->horizontalSlider->setMaximum(w->getImgListSize() - 1);
	ui->horizontalSlider->setValue(ui->horizontalSlider->maximum());
	ui->loadingLabel->setHidden(true);
	ui->loadingLabel->setText("LOADING...");
}
void ImageViewer::semiImplicitRegPM_clicked()
{
	if (!isImgOpened()) {
		msgBox.setText("No image is opened.");
		msgBox.setIcon(QMessageBox::Information);
		msgBox.exec();
		return;
	}
	if (ui->nTauRegPMSpinBox->value() < ui->nSaveRegPMSpinBox->value()) {
		msgBox.setText("Number of iterations is too small");
		msgBox.setIcon(QMessageBox::Information);
		msgBox.exec();
		ui->nTauRegPMSpinBox->setValue(ui->nSaveRegPMSpinBox->value());
	}
	ui->loadingLabel->setText("LOADING STEP:   " + QString::number(1));
	ui->loadingLabel->setVisible(true);
	ui->horizontalSlider->setVisible(true);
	qApp->processEvents();

	ViewerWidget* w = getCurrentViewerWidget();
	uchar* data = w->getData();
	Filters f;
	int k = 1;
	int depth = w->getImage()->depth();
	int rowNew, row = w->getImage()->bytesPerLine();
	if (depth == 8)
		rowNew = row + (k + 1) * 2;
	else
		rowNew = row + (k + 1) * 2 * 3;

	w->calcHistogramBW();
	std::cout << w->getMeanBW() << "\n";

	w->clearImgList();
	QVector<int> U0 = mirroring(k), Un;
	int nSteps = round(ui->nTauRegPMSpinBox->value() / ui->nSaveRegPMSpinBox->value());

	for (int step = 0; step < nSteps; step++) {
		Un = f.semiImplicitRegPM(U0, ui->sigmaRegPMSpinBox->value(), ui->tauRegPMSpinBox->value(), ui->nSaveRegPMSpinBox->value(), w->getImgWidth(), w->getImgHeight(), rowNew, false);
		if (depth == 8)
			for (int i = k; i < k + w->getImgHeight(); i++)
				for (int j = k; j < k + w->getImgWidth(); j++)
					w->setPixel(w->getImage(), j - k, i - k, static_cast<uchar>(Un[i * rowNew + j]));
		else {
			Un = f.semiImplicitRegPM(U0, ui->sigmaRegPMSpinBox->value(), ui->tauRegPMSpinBox->value(), ui->nSaveRegPMSpinBox->value(), w->getImgWidth(), w->getImgHeight(), rowNew, true);
			for (int i = k; i < k + w->getImgHeight(); i++)
				for (int j = k; j < k + w->getImgWidth(); j++) {
					w->setPixel(w->getImage(), j - k, i - k, static_cast<uchar>(Un[i * rowNew + j * 3]), static_cast<uchar>(Un[i * rowNew + j * 3 + 1]), static_cast<uchar>(Un[i * rowNew + j * 3 + 2]));
				}
		}
		w->saveIteration();
		U0 = Un;
		w->update();
		w->calcHistogramBW();
		std::cout << w->getMeanBW() << "\n";
		ui->loadingLabel->setText("LOADING STEP:   " + QString::number(step + 2));
		qApp->processEvents();
	}
	int mod = ui->nTauRegPMSpinBox->value() % ui->nSaveRegPMSpinBox->value();
	if (mod != 0) {
		Un = f.semiImplicitRegPM(U0,  ui->sigmaRegPMSpinBox->value(), ui->tauRegPMSpinBox->value(), mod, w->getImgWidth(), w->getImgHeight(), rowNew, false);
		if (depth == 8)
			for (int i = k; i < k + w->getImgHeight(); i++)
				for (int j = k; j < k + w->getImgWidth(); j++)
					w->setPixel(w->getImage(), j - k, i - k, static_cast<uchar>(Un[i * rowNew + j]));
		else {
			Un = f.semiImplicitRegPM(U0, ui->sigmaRegPMSpinBox->value(), ui->tauRegPMSpinBox->value(), mod, w->getImgWidth(), w->getImgHeight(), rowNew, true);
			for (int i = k; i < k + w->getImgHeight(); i++)
				for (int j = k; j < k + w->getImgWidth(); j++)
					w->setPixel(w->getImage(), j - k, i - k, static_cast<uchar>(Un[i * rowNew + j * 3]), static_cast<uchar>(Un[i * rowNew + j * 3 + 1]), static_cast<uchar>(Un[i * rowNew + j * 3 + 2]));
		}
		w->saveIteration();
		w->update();
		w->calcHistogramBW();
		std::cout << w->getMeanBW() << "\n";
	}

	ui->horizontalSlider->setMaximum(w->getImgListSize() - 1);
	ui->horizontalSlider->setValue(ui->horizontalSlider->maximum());
	ui->loadingLabel->setHidden(true);
	ui->loadingLabel->setText("LOADING...");
}
void ImageViewer::semiImplicitMCF_clicked()
{
	if (!isImgOpened()) {
		msgBox.setText("No image is opened.");
		msgBox.setIcon(QMessageBox::Information);
		msgBox.exec();
		return;
	}
	if (ui->nTauMCFSpinBox->value() < ui->nSaveMCFSpinBox->value()) {
		msgBox.setText("Number of iterations is too small");
		msgBox.setIcon(QMessageBox::Information);
		msgBox.exec();
		ui->nTauMCFSpinBox->setValue(ui->nSaveMCFSpinBox->value());
	}
	ui->loadingLabel->setText("LOADING STEP:   " + QString::number(1));
	ui->loadingLabel->setVisible(true);
	ui->horizontalSlider->setVisible(true);
	qApp->processEvents();

	ViewerWidget* w = getCurrentViewerWidget();
	uchar* data = w->getData();
	Filters f;
	int k = 1;
	int depth = w->getImage()->depth();
	int rowNew, row = w->getImage()->bytesPerLine();
	if (depth == 8)
		rowNew = row + (k + 1) * 2;
	else
		rowNew = row + (k + 1) * 2 * 3;

	w->calcHistogramBW();
	std::cout << w->getMeanBW() << "\n";

	w->clearImgList();
	QVector<int> U0 = mirroring(k), Un;
	int nSteps = round(ui->nTauMCFSpinBox->value() / ui->nSaveMCFSpinBox->value());

	for (int step = 0; step < nSteps; step++) {
		Un = f.semiImplicitMCF(U0, ui->tauMCFSpinBox->value(), ui->nSaveMCFSpinBox->value(), w->getImgWidth(), w->getImgHeight(), rowNew, false);
		if (depth == 8)
			for (int i = k; i < k + w->getImgHeight(); i++)
				for (int j = k; j < k + w->getImgWidth(); j++)
					w->setPixel(w->getImage(), j - k, i - k, static_cast<uchar>(Un[i * rowNew + j]));
		else {
			Un = f.semiImplicitMCF(U0, ui->tauMCFSpinBox->value(), ui->nSaveMCFSpinBox->value(), w->getImgWidth(), w->getImgHeight(), rowNew, true);
			for (int i = k; i < k + w->getImgHeight(); i++)
				for (int j = k; j < k + w->getImgWidth(); j++) {
					w->setPixel(w->getImage(), j - k, i - k, static_cast<uchar>(Un[i * rowNew + j * 3]), static_cast<uchar>(Un[i * rowNew + j * 3 + 1]), static_cast<uchar>(Un[i * rowNew + j * 3 + 2]));
				}
		}
		w->saveIteration();
		U0 = Un;
		w->update();
		w->calcHistogramBW();
		std::cout << w->getMeanBW() << "\n";
		ui->loadingLabel->setText("LOADING STEP:   " + QString::number(step + 2));
		qApp->processEvents();
	}
	int mod = ui->nTauMCFSpinBox->value() % ui->nSaveMCFSpinBox->value();
	if (mod != 0) {
		Un = f.semiImplicitMCF(U0, ui->tauMCFSpinBox->value(), mod, w->getImgWidth(), w->getImgHeight(), rowNew, false);
		if (depth == 8)
			for (int i = k; i < k + w->getImgHeight(); i++)
				for (int j = k; j < k + w->getImgWidth(); j++)
					w->setPixel(w->getImage(), j - k, i - k, static_cast<uchar>(Un[i * rowNew + j]));
		else {
			Un = f.semiImplicitMCF(U0, ui->tauMCFSpinBox->value(), mod, w->getImgWidth(), w->getImgHeight(), rowNew, true);
			for (int i = k; i < k + w->getImgHeight(); i++)
				for (int j = k; j < k + w->getImgWidth(); j++)
					w->setPixel(w->getImage(), j - k, i - k, static_cast<uchar>(Un[i * rowNew + j * 3]), static_cast<uchar>(Un[i * rowNew + j * 3 + 1]), static_cast<uchar>(Un[i * rowNew + j * 3 + 2]));
		}
		w->saveIteration();
		w->update();
		w->calcHistogramBW();
		std::cout << w->getMeanBW() << "\n";
	}

	ui->horizontalSlider->setMaximum(w->getImgListSize() - 1);
	ui->horizontalSlider->setValue(ui->horizontalSlider->maximum());
	ui->loadingLabel->setHidden(true);
	ui->loadingLabel->setText("LOADING...");
}
void ImageViewer::semiImplicitGMCF_clicked()
{
	if (!isImgOpened()) {
		msgBox.setText("No image is opened.");
		msgBox.setIcon(QMessageBox::Information);
		msgBox.exec();
		return;
	}
	if (ui->nTauGMCFSpinBox->value() < ui->nSaveGMCFSpinBox->value()) {
		msgBox.setText("Number of iterations is too small");
		msgBox.setIcon(QMessageBox::Information);
		msgBox.exec();
		ui->nTauGMCFSpinBox->setValue(ui->nSaveGMCFSpinBox->value());
	}
	ui->loadingLabel->setText("LOADING STEP:   " + QString::number(1));
	ui->loadingLabel->setVisible(true);
	ui->horizontalSlider->setVisible(true);
	qApp->processEvents();

	ViewerWidget* w = getCurrentViewerWidget();
	uchar* data = w->getData();
	Filters f;
	int k = 1;
	int depth = w->getImage()->depth();
	int rowNew, row = w->getImage()->bytesPerLine();
	if (depth == 8)
		rowNew = row + (k + 1) * 2;
	else
		rowNew = row + (k + 1) * 2 * 3;

	w->calcHistogramBW();
	std::cout << w->getMeanBW() << "\n";

	w->clearImgList();
	QVector<int> U0 = mirroring(k), Un;
	int nSteps = round(ui->nTauGMCFSpinBox->value() / ui->nSaveGMCFSpinBox->value());

	for (int step = 0; step < nSteps; step++) {
		Un = f.semiImplicitGMCF(U0, ui->sigmaGMCFSpinBox->value(), ui->tauGMCFSpinBox->value(), ui->nSaveGMCFSpinBox->value(), w->getImgWidth(), w->getImgHeight(), rowNew, false);
		if (depth == 8)
			for (int i = k; i < k + w->getImgHeight(); i++)
				for (int j = k; j < k + w->getImgWidth(); j++)
					w->setPixel(w->getImage(), j - k, i - k, static_cast<uchar>(Un[i * rowNew + j]));
		else {
			Un = f.semiImplicitGMCF(U0, ui->sigmaGMCFSpinBox->value(), ui->tauGMCFSpinBox->value(), ui->nSaveGMCFSpinBox->value(), w->getImgWidth(), w->getImgHeight(), rowNew, true);
			for (int i = k; i < k + w->getImgHeight(); i++)
				for (int j = k; j < k + w->getImgWidth(); j++) {
					w->setPixel(w->getImage(), j - k, i - k, static_cast<uchar>(Un[i * rowNew + j * 3]), static_cast<uchar>(Un[i * rowNew + j * 3 + 1]), static_cast<uchar>(Un[i * rowNew + j * 3 + 2]));
				}
		}
		w->saveIteration();
		U0 = Un;
		w->update();
		w->calcHistogramBW();
		std::cout << w->getMeanBW() << "\n";
		ui->loadingLabel->setText("LOADING STEP:   " + QString::number(step + 2));
		qApp->processEvents();
	}
	int mod = ui->nTauGMCFSpinBox->value() % ui->nSaveGMCFSpinBox->value();
	if (mod != 0) {
		Un = f.semiImplicitGMCF(U0, ui->sigmaGMCFSpinBox->value(), ui->tauGMCFSpinBox->value(), mod , w->getImgWidth(), w->getImgHeight(), rowNew, false);
		if (depth == 8)
			for (int i = k; i < k + w->getImgHeight(); i++)
				for (int j = k; j < k + w->getImgWidth(); j++)
					w->setPixel(w->getImage(), j - k, i - k, static_cast<uchar>(Un[i * rowNew + j]));
		else {
			Un = f.semiImplicitGMCF(U0, ui->sigmaGMCFSpinBox->value(), ui->tauGMCFSpinBox->value(), mod, w->getImgWidth(), w->getImgHeight(), rowNew, true);
			for (int i = k; i < k + w->getImgHeight(); i++)
				for (int j = k; j < k + w->getImgWidth(); j++)
					w->setPixel(w->getImage(), j - k, i - k, static_cast<uchar>(Un[i * rowNew + j * 3]), static_cast<uchar>(Un[i * rowNew + j * 3 + 1]), static_cast<uchar>(Un[i * rowNew + j * 3 + 2]));
		}
		w->saveIteration();
		w->update();
		w->calcHistogramBW();
		std::cout << w->getMeanBW() << "\n";
	}

	ui->horizontalSlider->setMaximum(w->getImgListSize() - 1);
	ui->horizontalSlider->setValue(ui->horizontalSlider->maximum());
	ui->loadingLabel->setHidden(true);
	ui->loadingLabel->setText("LOADING...");
}
void ImageViewer::distanceFunction_clicked()
{
	if (!isImgOpened()) {
		msgBox.setText("No image is opened.");
		msgBox.setIcon(QMessageBox::Information);
		msgBox.exec();
		return;
	}
	if (ui->nTauGMCFSpinBox->value() < ui->nSaveGMCFSpinBox->value()) {
		msgBox.setText("Number of iterations is too small");
		msgBox.setIcon(QMessageBox::Information);
		msgBox.exec();
		ui->nTauGMCFSpinBox->setValue(ui->nSaveGMCFSpinBox->value());
	}
	ui->loadingLabel->setText("LOADING STEP:   " + QString::number(1));
	ui->loadingLabel->setVisible(true);
	ui->horizontalSlider->setVisible(true);
	qApp->processEvents();
	
	OtsuThreshold();

	ViewerWidget* w = getCurrentViewerWidget();
	uchar* data = w->getData();
	Filters f;
	int k = 1;
	int depth = w->getImage()->depth();
	int rowNew, row = w->getImage()->bytesPerLine();
	if (depth == 8)
		rowNew = row + (k + 1) * 2;
	else
		rowNew = row + (k + 1) * 2 * 3;

	w->clearImgList();
	QVector<int> U0 = mirroring(k);
	QVector<double> Un;

	Un = f.ruoyTourin(f.contour(U0, w->getImgWidth(), w->getImgHeight(), rowNew), 0.4, w->getImgWidth(), w->getImgHeight(), rowNew);
	if (depth == 8)
		for (int i = k; i < k + w->getImgHeight(); i++)
			for (int j = k; j < k + w->getImgWidth(); j++)
				w->setPixel(w->getImage(), j - k, i - k, static_cast<uchar>(round(Un[i * rowNew + j])));
	else {
		msgBox.setText("wtf.");
		msgBox.setIcon(QMessageBox::Information);
		msgBox.exec();
	}
	FSHS();
	w->update();

	ui->horizontalSlider->setMaximum(w->getImgListSize() - 1);
	ui->horizontalSlider->setValue(ui->horizontalSlider->maximum());
	ui->loadingLabel->setHidden(true);
	ui->loadingLabel->setText("LOADING...");
}
void ImageViewer::signedDistFunction_clicked()
{
	if (!isImgOpened()) {
		msgBox.setText("No image is opened.");
		msgBox.setIcon(QMessageBox::Information);
		msgBox.exec();
		return;
	}
	if (ui->nTauGMCFSpinBox->value() < ui->nSaveGMCFSpinBox->value()) {
		msgBox.setText("Number of iterations is too small");
		msgBox.setIcon(QMessageBox::Information);
		msgBox.exec();
		ui->nTauGMCFSpinBox->setValue(ui->nSaveGMCFSpinBox->value());
	}
	ui->loadingLabel->setText("LOADING STEP:   " + QString::number(1));
	ui->loadingLabel->setVisible(true);
	ui->horizontalSlider->setVisible(true);
	qApp->processEvents();

	OtsuThreshold();

	ViewerWidget* w = getCurrentViewerWidget();
	uchar* data = w->getData();
	Filters f;
	int k = 1;
	int depth = w->getImage()->depth();
	int rowNew, row = w->getImage()->bytesPerLine();
	if (depth == 8)
		rowNew = row + (k + 1) * 2;
	else
		rowNew = row + (k + 1) * 2 * 3;

	w->clearImgList();
	QVector<int> U0 = mirroring(k);
	QVector<double> Un, Udist;

	Udist = f.ruoyTourin(f.contour(U0, w->getImgWidth(), w->getImgHeight(), rowNew), 0.4, w->getImgWidth(), w->getImgHeight(), rowNew);
	Un = f.signedDistFunction(U0, Udist , w->getImgWidth(), w->getImgHeight(), rowNew);

	if (depth == 8)
		for (int i = k; i < k + w->getImgHeight(); i++)
			for (int j = k; j < k + w->getImgWidth(); j++)
				w->setPixel(w->getImage(), j - k, i - k, static_cast<uchar>(round(Un[i * rowNew + j])));
	else {
		msgBox.setText("wtf.");
		msgBox.setIcon(QMessageBox::Information);
		msgBox.exec();
	}
	w->update();

	ui->horizontalSlider->setMaximum(w->getImgListSize() - 1);
	ui->horizontalSlider->setValue(ui->horizontalSlider->maximum());
	ui->loadingLabel->setHidden(true);
	ui->loadingLabel->setText("LOADING...");
}
void ImageViewer::subsurf_clicked()
{
	if (!isImgOpened()) {
		msgBox.setText("No image is opened.");
		msgBox.setIcon(QMessageBox::Information);
		msgBox.exec();
		return;
	}
	if (ui->nTauGMCFSpinBox->value() < ui->nSaveGMCFSpinBox->value()) {
		msgBox.setText("Number of iterations is too small");
		msgBox.setIcon(QMessageBox::Information);
		msgBox.exec();
		ui->nTauGMCFSpinBox->setValue(ui->nSaveGMCFSpinBox->value());
	}
	ui->loadingLabel->setText("LOADING STEP:   " + QString::number(1));
	ui->loadingLabel->setVisible(true);
	ui->horizontalSlider->setVisible(true);
	qApp->processEvents();

	ViewerWidget* w = getCurrentViewerWidget();
	uchar* data = w->getData();
	Filters f;
	int k = 1;
	int depth = w->getImage()->depth();
	int rowNew, row = w->getImage()->bytesPerLine();
	if (depth == 8)
		rowNew = row + (k + 1) * 2;
	else
		rowNew = row + (k + 1) * 2 * 3;

	OtsuThreshold();
	
	w->calcHistogramBW();
	std::cout << w->getMeanBW() << "\n";

	w->clearImgList();
	QVector<int> U0 = mirroring(k), Un;
	int nSteps = round(ui->nTauSubsurfSpinBox->value() / ui->nSaveSubsurfSpinBox->value());

	for (int step = 0; step < nSteps; step++) {
		Un = f.subsurf(U0, ui->sigmaSubsurfSpinBox->value(), ui->tauSubsurfSpinBox->value(), ui->nSaveSubsurfSpinBox->value(), w->getImgWidth(), w->getImgHeight(), rowNew, false);
		if (depth == 8)
			for (int i = k; i < k + w->getImgHeight(); i++)
				for (int j = k; j < k + w->getImgWidth(); j++) {
					//std::cout << Un[i * rowNew + j] << "\t";
					if (Un[i * rowNew + j] >= 255)
						w->setPixel(w->getImage(), j - k, i - k, static_cast<uchar>(255));
					else
						w->setPixel(w->getImage(), j - k, i - k, static_cast<uchar>(0));
				}
		else {
			Un = f.subsurf(U0, ui->sigmaSubsurfSpinBox->value(), ui->tauSubsurfSpinBox->value(), ui->nSaveSubsurfSpinBox->value(), w->getImgWidth(), w->getImgHeight(), rowNew, true);
			for (int i = k; i < k + w->getImgHeight(); i++)
				for (int j = k; j < k + w->getImgWidth(); j++) {
					w->setPixel(w->getImage(), j - k, i - k, static_cast<uchar>(Un[i * rowNew + j * 3]), static_cast<uchar>(Un[i * rowNew + j * 3 + 1]), static_cast<uchar>(Un[i * rowNew + j * 3 + 2]));
				}
		}
		w->saveIteration();
		U0 = Un;
		w->update();
		w->calcHistogramBW();
		std::cout << w->getMeanBW() << "\n";
		ui->loadingLabel->setText("LOADING STEP:   " + QString::number(step + 2));
		qApp->processEvents();
	}
	int mod = ui->nTauSubsurfSpinBox->value() % ui->nSaveSubsurfSpinBox->value();
	if (mod != 0) {
		Un = f.subsurf(U0, ui->sigmaSubsurfSpinBox->value(), ui->tauSubsurfSpinBox->value(), mod, w->getImgWidth(), w->getImgHeight(), rowNew, false);
		if (depth == 8)
			for (int i = k; i < k + w->getImgHeight(); i++)
				for (int j = k; j < k + w->getImgWidth(); j++)
					w->setPixel(w->getImage(), j - k, i - k, static_cast<uchar>(Un[i * rowNew + j]));
		else {
			Un = f.subsurf(U0, ui->sigmaSubsurfSpinBox->value(), ui->tauSubsurfSpinBox->value(), mod, w->getImgWidth(), w->getImgHeight(), rowNew, true);
			for (int i = k; i < k + w->getImgHeight(); i++)
				for (int j = k; j < k + w->getImgWidth(); j++)
					w->setPixel(w->getImage(), j - k, i - k, static_cast<uchar>(Un[i * rowNew + j * 3]), static_cast<uchar>(Un[i * rowNew + j * 3 + 1]), static_cast<uchar>(Un[i * rowNew + j * 3 + 2]));
		}
		w->saveIteration();
		w->update();
		w->calcHistogramBW();
		std::cout << w->getMeanBW() << "\n";
	}
	
	ui->horizontalSlider->setMaximum(w->getImgListSize() - 1);
	ui->horizontalSlider->setValue(ui->horizontalSlider->maximum());
	ui->loadingLabel->setHidden(true);
	ui->loadingLabel->setText("LOADING...");
}
void ImageViewer::slider_moved(int i)
{
	ViewerWidget* w = getCurrentViewerWidget();
	if (w->getImgListSize() != 0)
		w->showImgFromList(i);
	else
		ui->horizontalSlider->setHidden(true);
}
void ImageViewer::showTools_clicked()
{
	ui->dockWidget->setVisible(true);
	ui->horizontalSlider->setVisible(true);

}
void ImageViewer::hideTools_clicked()
{
	ui->dockWidget->setHidden(true);
	ui->horizontalSlider->setHidden(true);
}