#include "Filters.h"



Filters::Filters()
{
}


Filters::~Filters()
{
}

QVector<int> Filters::explicitLinDif(QVector<int> U0, double tau, int n,int w, int h, int row, bool rgb)
{
	QVector<double> Un(U0.size());
	QVector<double> Uprev(U0.size());
	for (int i = 0; i < U0.size(); i++)
		Uprev[i] = U0[i]/255.0;

	if (rgb) {/*
		int sumUq_R, sumUq_G, sumUq_B;
		double newIntensity_R, newIntensity_G, newIntensity_B;
		for (int step = 0; step < n; step++) {
			for (int i = 1; i < 1 + h; i++) {
				for (int j = 1; j < 1 + w; j++) {
					sumUq_R = U0[i * row + (j - 1) * 3] + U0[i * row + (j + 1)*3] + U0[i * row + j + row] + U0[i * row + j - row];
					sumUq_G = U0[i * row + (j - 1) * 3 + 1] + U0[i * row + (j + 1) * 3 + 1] + U0[i * row + j + row+ 1] + U0[i * row + j - row + 1];
					sumUq_B = U0[i * row + (j - 1) * 3 + 2] + U0[i * row + (j + 1) * 3 + 2] + U0[i * row + j + row + 2] + U0[i * row + j - row + 2];
					newIntensity_R = (1 - tau * 4) * U0[i * row + j * 3] + tau * sumUq_R;
					newIntensity_G = (1 - tau * 4) * U0[i * row + j * 3 + 1] + tau * sumUq_G;
					newIntensity_B = (1 - tau * 4) * U0[i * row + j * 3 + 2] + tau * sumUq_B;
					Un[i * row + j *3] = round(newIntensity_R);
					Un[i * row + j * 3 + 1] = round(newIntensity_G);
					Un[i * row + j * 3 + 2] = round(newIntensity_B);
				}
			}
			for (int j = 1; j < 1 + w; j++) {	//mirroring by 1pixel
				Un[j * 3] = Un[j + row];							//top
				Un[j * 3 + 1] = Un[j + row + 1];
				Un[j * 3 + 2] = Un[j + row + 2];
				Un[(1 + h) * row + j * 3] = Un[h * row + j * 3];		//bottom
				Un[(1 + h) * row + j * 3 + 1] = Un[h * row + j * 3 + 1];
				Un[(1 + h) * row + j * 3 + 2] = Un[h * row + j * 3 + 2];
			}
			for (int i = 0; i < 2 + h; i++) {
				Un[i * row] = Un[i * row + 3];							//left
				Un[i * row + 1] = Un[i * row + 4];
				Un[i * row + 2] = Un[i * row + 5];
				Un[i * row + (1 + w)*3] = Un[i * row + w * 3];			//right
				Un[i * row + (1 + w) * 3 + 1] = Un[i * row + w * 3 + 1];
				Un[i * row + (1 + w) * 3 + 2] = Un[i * row + w * 3 + 2];
			}
			U0 = Un;
		}*/
	}
	else {
		double sumUq;
		double newIntensity;
		for (int step = 0; step < n; step++) {
			for (int i = 1; i < 1 + h; i++) {
				for (int j = 1; j < 1 + w; j++) {
					sumUq = Uprev[i * row + j - 1] + Uprev[i * row + j + 1] + Uprev[i * row + j + row] + Uprev[i * row + j - row];
					newIntensity = (1 - tau * 4) * Uprev[i * row + j] + tau * sumUq;
					Un[i * row + j] = newIntensity;
				}
			}
			for (int j = 1; j < 1 + w; j++) {
				Un[j] = Un[j + row];									//top
				Un[(1 + h) * row + j] = Un[h * row + j];				//bottom
			}
			for (int i = 0; i < 2 + h; i++) {
				Un[i * row] = Un[i * row + 1];							//left
				Un[i * row + (1 + w)] = Un[i * row + w];				//right
			}
			Uprev = Un;
		}
	}
	for (int i = 0; i < U0.size(); i++)
		U0[i] = round(Un[i]*255.0);

	return U0;
}

QVector<int> Filters::implicitLinDif(QVector<int> U0, double tau, int n, int w, int h, int row, bool rgb)
{
	QVector<double> Un(U0.size());
	QVector<double> Uprev(U0.size());
	double tol = 0.000001;
	double omega = 1.2;
	int iter, maxIter = 1000;
	double rezTMP, rez = 100;
	double apq = tau; // tau/(h^2)	-> h=1

	for (int i = 0; i < U0.size(); i++)
		Uprev[i] = U0[i]/255.0;

	if (rgb) {
		int sumUq_R, sumUq_G, sumUq_B;
		double newIntensity_R, newIntensity_G, newIntensity_B;
	}
	else {
		double sumUq;
		double newIntensity, y;
		
		for (int step = 0; step < n; step++) {
			iter = 0;
			Un = Uprev;
			while (rez > tol) {				
				for (int i = 1; i < 1 + h; i++) {
					for (int j = 1; j < 1 + w; j++) {
						sumUq = Un[i * row + j - 1] + Un[i * row + j + 1] + Un[i * row + j + row] + Un[i * row + j - row];
						y = (Uprev[i * row + j] + apq * sumUq) / (double)(1 + 4 * apq);		//Gauss-Seidel
						newIntensity = SOR(y, Un[i * row + j], omega);						//SOR
						Un[i * row + j] = newIntensity;
					}
				}
				for (int j = 1; j < 1 + w; j++) {
					Un[j] = Un[j + row];									//top
					Un[(1 + h) * row + j] = Un[h * row + j];				//bottom
				}
				for (int i = 0; i < 2 + h; i++) {
					Un[i * row] = Un[i * row + 1];							//left
					Un[i * row + (1 + w)] = Un[i * row + w];				//right
				}
				rez = 0;
				for (int i = 1; i < 1 + h; i++) {
					for (int j = 1; j < 1 + w; j++) {
						sumUq = Un[i * row + j - 1] + Un[i * row + j + 1] + Un[i * row + j + row] + Un[i * row + j - row];
						rezTMP = (Uprev[i * row + j] + apq * sumUq) / (double)(1 + 4 * apq) - Un[i * row + j];
						rez += rezTMP * rezTMP;
					}
				}
				iter++;
				if (iter >= maxIter)
					break;
			}
			Uprev = Un;
		}
	}
	for (int i = 0; i < U0.size(); i++)
		U0[i] = round(Un[i]*255.0);

	return U0;
}

QVector<int> Filters::explicitPM(QVector<int> U0, double tau, int n, int w, int h, int row, bool rgb)
{
	QMessageBox msgBox;
	QVector<double> Un(U0.size());
	QVector<double> Uprev(U0.size());

	for (int i = 0; i < U0.size(); i++)
		Uprev[i] = U0[i]/255.0;

	if (rgb) {/*
		int sumUq_R, sumUq_G, sumUq_B;
		double newIntensity_R, newIntensity_G, newIntensity_B;
		for (int step = 0; step < n; step++) {
			for (int i = 1; i < 1 + h; i++) {
				for (int j = 1; j < 1 + w; j++) {
					sumUq_R = U0[i * row + (j - 1) * 3] + U0[i * row + (j + 1) * 3] + U0[i * row + j + row] + U0[i * row + j - row];
					sumUq_G = U0[i * row + (j - 1) * 3 + 1] + U0[i * row + (j + 1) * 3 + 1] + U0[i * row + j + row + 1] + U0[i * row + j - row + 1];
					sumUq_B = U0[i * row + (j - 1) * 3 + 2] + U0[i * row + (j + 1) * 3 + 2] + U0[i * row + j + row + 2] + U0[i * row + j - row + 2];
					newIntensity_R = (1 - tau * 4) * U0[i * row + j * 3] + tau * sumUq_R;
					newIntensity_G = (1 - tau * 4) * U0[i * row + j * 3 + 1] + tau * sumUq_G;
					newIntensity_B = (1 - tau * 4) * U0[i * row + j * 3 + 2] + tau * sumUq_B;
					Un[i * row + j * 3] = round(newIntensity_R);
					Un[i * row + j * 3 + 1] = round(newIntensity_G);
					Un[i * row + j * 3 + 2] = round(newIntensity_B);
				}
			}
			for (int j = 1; j < 1 + w; j++) {	//mirroring by 1pixel
				Un[j * 3] = Un[j + row];							//top
				Un[j * 3 + 1] = Un[j + row + 1];
				Un[j * 3 + 2] = Un[j + row + 2];
				Un[(1 + h) * row + j * 3] = Un[h * row + j * 3];		//bottom
				Un[(1 + h) * row + j * 3 + 1] = Un[h * row + j * 3 + 1];
				Un[(1 + h) * row + j * 3 + 2] = Un[h * row + j * 3 + 2];
			}
			for (int i = 0; i < 2 + h; i++) {
				Un[i * row] = Un[i * row + 3];							//left
				Un[i * row + 1] = Un[i * row + 4];
				Un[i * row + 2] = Un[i * row + 5];
				Un[i * row + (1 + w) * 3] = Un[i * row + w * 3];			//right
				Un[i * row + (1 + w) * 3 + 1] = Un[i * row + w * 3 + 1];
				Un[i * row + (1 + w) * 3 + 2] = Un[i * row + w * 3 + 2];
			}
			U0 = Un;
		}*/
	}
	else {
		double sumGpq, sumGpqUq;
		double newIntensity;
		double Uw_x, Uw_y, Ue_x, Ue_y, Us_x, Us_y, Un_x, Un_y;
		for (int step = 0; step < n; step++) {
			for (int i = 1; i < 1 + h; i++) {
				for (int j = 1; j < 1 + w; j++) {
					//DIAMOND CELL
					Uw_x = Uprev[i * row + j + 1] - Uprev[i * row + j];		//west
					Uw_y = (Uprev[(i - 1) * row + j] + Uprev[(i - 1) * row + j + 1] - Uprev[(i + 1) * row + j] - Uprev[(i + 1) * row + j + 1]) /4.0;
					Un_y = Uprev[(i - 1) * row + j] - Uprev[i * row + j];		//north
					Un_x = (Uprev[(i - 1) * row + j - 1] + Uprev[i * row + j - 1] - Uprev[(i - 1) * row + j + 1] - Uprev[i * row + j + 1]) / 4.0;
					Ue_x = Uprev[i * row + j - 1] - Uprev[i * row + j];		//east
					Ue_y = (Uprev[(i + 1) * row + j - 1] + Uprev[(i + 1) * row + j] - Uprev[(i - 1) * row + j - 1] - Uprev[(i - 1) * row + j]) / 4.0;
					Us_y = Uprev[(i + 1) * row + j] - Uprev[i * row + j];		//south
					Us_x = (Uprev[i * row + j + 1] + Uprev[(i + 1) * row + j + 1] - Uprev[i * row + j - 1] - Uprev[(i + 1) * row + j - 1]) / 4.0;

					sumGpq = g(normGrad(Uw_x, Uw_y)) + g(normGrad(Un_x, Un_y)) + g(normGrad(Ue_x, Ue_y)) + g(normGrad(Us_x, Us_y));
					sumGpqUq = g(normGrad(Uw_x, Uw_y)) * Uprev[i * row + j + 1] + g(normGrad(Un_x, Un_y)) * Uprev[i * row + j - row] + g(normGrad(Ue_x, Ue_y)) * Uprev[i * row + j - 1] + g(normGrad(Us_x, Us_y)) * Uprev[i * row + j + row];
					newIntensity = (1 - tau * sumGpq) * Uprev[i * row + j] + tau * sumGpqUq;
					Un[i * row + j] = newIntensity;
				}
			}
			for (int j = 1; j < 1 + w; j++) {
				Un[j] = Un[j + row];									//top
				Un[(1 + h) * row + j] = Un[h * row + j];				//bottom
			}
			for (int i = 0; i < 2 + h; i++) {
				Un[i * row] = Un[i * row + 1];							//left
				Un[i * row + (1 + w)] = Un[i * row + w];				//right
			}
			Uprev = Un;
		}
	}
	for (int i = 0; i < U0.size(); i++)
		U0[i] = round(Un[i] * 255);
	return U0;
}

QVector<int> Filters::semiImplicitPM(QVector<int> U0, double tau, int n, int w, int h, int row, bool rgb)
{
	QVector<double> Un(U0.size());
	QVector<double> Uprev(U0.size());
	QMessageBox msgBox;
	double tol = 0.000001;
	double omega = 1.2;
	int iter, maxIter = 1000;
	double rezTMP, rez = 100;

	for (int i = 0; i < U0.size(); i++)
		Uprev[i] = U0[i] / 255.0;

	if (rgb) {
		//int sumUq_R, sumUq_G, sumUq_B;
		//double newIntensity_R, newIntensity_G, newIntensity_B;
	}
	else {
		double sumGpq, sumGpqUq, sumUq;
		double newIntensity, y;

		for (int step = 0; step < n; step++) {
			iter = 0;
			Un = Uprev;
			QVector< QVector <double> > Ugrad(U0.size());
			for (int i = 1; i < 1 + h; i++) {													//CALCULATE GRADIENTS
				for (int j = 1; j < 1 + w; j++) {
					Ugrad[i * row + j].resize(8);												//DIAMOND CELL
					Ugrad[i * row + j][0] = Uprev[i * row + j + 1] - Uprev[i * row + j];																					//west
					Ugrad[i * row + j][1] = (Uprev[(i - 1) * row + j] + Uprev[(i - 1) * row + j + 1] - Uprev[(i + 1) * row + j] - Uprev[(i + 1) * row + j + 1]) / 4.0;
					Ugrad[i * row + j][2] = (Uprev[(i - 1) * row + j - 1] + Uprev[i * row + j - 1] - Uprev[(i - 1) * row + j + 1] - Uprev[i * row + j + 1]) / 4.0;			//north
					Ugrad[i * row + j][3] = Uprev[(i - 1) * row + j] - Uprev[i * row + j];
					Ugrad[i * row + j][4] = Uprev[i * row + j - 1] - Uprev[i * row + j];																					//east
					Ugrad[i * row + j][5] = (Uprev[(i + 1) * row + j - 1] + Uprev[(i + 1) * row + j] - Uprev[(i - 1) * row + j - 1] - Uprev[(i - 1) * row + j]) / 4.0;
					Ugrad[i * row + j][6] = (Uprev[i * row + j + 1] + Uprev[(i + 1) * row + j + 1] - Uprev[i * row + j - 1] - Uprev[(i + 1) * row + j - 1]) / 4.0;			//south
					Ugrad[i * row + j][7] = Uprev[(i + 1) * row + j] - Uprev[i * row + j];
				}
			}
			while (rez > tol) {
				for (int i = 1; i < 1 + h; i++) {
					for (int j = 1; j < 1 + w; j++) {
						sumGpq = g(normGrad(Ugrad[i * row + j][0], Ugrad[i * row + j][1])) + g(normGrad(Ugrad[i * row + j][2], Ugrad[i * row + j][3])) + g(normGrad(Ugrad[i * row + j][4], Ugrad[i * row + j][5])) + g(normGrad(Ugrad[i * row + j][6], Ugrad[i * row + j][7]));
						sumGpqUq = g(normGrad(Ugrad[i * row + j][0], Ugrad[i * row + j][1])) * Un[i * row + j + 1] + g(normGrad(Ugrad[i * row + j][2], Ugrad[i * row + j][3])) * Un[i * row + j - row] + g(normGrad(Ugrad[i * row + j][4], Ugrad[i * row + j][5])) * Un[i * row + j - 1] + g(normGrad(Ugrad[i * row + j][6], Ugrad[i * row + j][7])) * Un[i * row + j + row];

						y = (Uprev[i * row + j] + tau * sumGpqUq) / (double)(1 + tau*sumGpq);	//Gauss-Seidel
						newIntensity = SOR(y, Un[i * row + j], omega);							//SOR
						Un[i * row + j] = newIntensity;
					}
				}																				//MIRRORING
				for (int j = 1; j < 1 + w; j++) {
					Un[j] = Un[j + row];														//top
					Un[(1 + h) * row + j] = Un[h * row + j];									//bottom
				}
				for (int i = 0; i < 2 + h; i++) {
					Un[i * row] = Un[i * row + 1];												//left
					Un[i * row + (1 + w)] = Un[i * row + w];									//right
				}
				rez = 0;
				for (int i = 1; i < 1 + h; i++) {
					for (int j = 1; j < 1 + w; j++) {
						sumGpq = g(normGrad(Ugrad[i * row + j][0], Ugrad[i * row + j][1])) + g(normGrad(Ugrad[i * row + j][2], Ugrad[i * row + j][3])) + g(normGrad(Ugrad[i * row + j][4], Ugrad[i * row + j][5])) + g(normGrad(Ugrad[i * row + j][6], Ugrad[i * row + j][7]));
						sumGpqUq = g(normGrad(Ugrad[i * row + j][0], Ugrad[i * row + j][1])) * Un[i * row + j + 1] + g(normGrad(Ugrad[i * row + j][2], Ugrad[i * row + j][3])) * Un[i * row + j - row] + g(normGrad(Ugrad[i * row + j][4], Ugrad[i * row + j][5])) * Un[i * row + j - 1] + g(normGrad(Ugrad[i * row + j][6], Ugrad[i * row + j][7])) * Un[i * row + j + row];

						rezTMP = ((Uprev[i * row + j] + tau * sumGpqUq) / (double)(1 + tau * sumGpq)) - Un[i * row + j];
						rez += rezTMP * rezTMP;
					}
				}
				iter++;
				if (iter >= maxIter)
					break;
			}
			Uprev = Un;
		}
	}
	for (int i = 0; i < U0.size(); i++)
		U0[i] = round(Un[i]*255.0);

	return U0;
}

QVector<int> Filters::semiImplicitRegPM(QVector<int> U0, double sigma, double tau, int n, int w, int h, int row, bool rgb)
{
	QVector<double> Un(U0.size());
	QVector<double> Uprev(U0.size());
	double tol = 0.000001;
	double omega = 1.2;
	int iter, maxIter = 1000;
	double rezTMP, rez = 100;

	for (int i = 0; i < U0.size(); i++)															//intensity values to 0-1
		Uprev[i] = U0[i] / 255.0;

	if (rgb) {
		//int sumUq_R, sumUq_G, sumUq_B;
		//double newIntensity_R, newIntensity_G, newIntensity_B;
	}
	else {
		double sumGpq, sumGpqUq, sumUq;
		double newIntensity, y;

		for (int step = 0; step < n; step++) {
			Un = Uprev;
			iter = 0;
			
			while (rez > tol) {																	//IMPLICIT LHE																	
				for (int i = 1; i < 1 + h; i++) {
					for (int j = 1; j < 1 + w; j++) {
						sumUq = Un[i * row + j - 1] + Un[i * row + j + 1] + Un[i * row + j + row] + Un[i * row + j - row];
						y = (Uprev[i * row + j] + sigma * sumUq) / (double)(1 + 4 * sigma);		//Gauss-Seidel
						newIntensity = SOR(y, Un[i * row + j], omega);							//SOR
						Un[i * row + j] = newIntensity;
					}
				}
				for (int j = 1; j < 1 + w; j++) {												//MIRRORING
					Un[j] = Un[j + row];														//top
					Un[(1 + h) * row + j] = Un[h * row + j];									//bottom
				}
				for (int i = 0; i < 2 + h; i++) {
					Un[i * row] = Un[i * row + 1];												//left
					Un[i * row + (1 + w)] = Un[i * row + w];									//right
				}
				rez = 0;
				for (int i = 1; i < 1 + h; i++) {												//CALCULATE RESIDUAL
					for (int j = 1; j < 1 + w; j++) {
						sumUq = Un[i * row + j - 1] + Un[i * row + j + 1] + Un[i * row + j + row] + Un[i * row + j - row];
						rezTMP = (Uprev[i * row + j] + sigma * sumUq) / (double)(1 + 4 * sigma) - Un[i * row + j];
						rez += rezTMP * rezTMP;
					}
				}
				iter++;
				if (iter >= maxIter)
					break;
			}
			QVector< QVector <double> > Ugrad(U0.size());
			for (int i = 1; i < 1 + h; i++) {													//CALCULATE GRADIENTS
				for (int j = 1; j < 1 + w; j++) {
					Ugrad[i * row + j].resize(8);												//DIAMOND CELL
					Ugrad[i * row + j][0] = Un[i * row + j + 1] - Un[i * row + j];																				//west
					Ugrad[i * row + j][1] = (Un[(i - 1) * row + j] + Un[(i - 1) * row + j + 1] - Un[(i + 1) * row + j] - Un[(i + 1) * row + j + 1]) / 4.0;
					Ugrad[i * row + j][2] = (Un[(i - 1) * row + j - 1] + Un[i * row + j - 1] - Un[(i - 1) * row + j + 1] - Un[i * row + j + 1]) / 4.0;			//north
					Ugrad[i * row + j][3] = Un[(i - 1) * row + j] - Un[i * row + j];
					Ugrad[i * row + j][4] = Un[i * row + j - 1] - Un[i * row + j];																				//east
					Ugrad[i * row + j][5] = (Un[(i + 1) * row + j - 1] + Un[(i + 1) * row + j] - Un[(i - 1) * row + j - 1] - Un[(i - 1) * row + j]) / 4.0;
					Ugrad[i * row + j][6] = (Un[i * row + j + 1] + Un[(i + 1) * row + j + 1] - Un[i * row + j - 1] - Un[(i + 1) * row + j - 1]) / 4.0;			//south
					Ugrad[i * row + j][7] = Un[(i + 1) * row + j] - Un[i * row + j];
				}
			}
			iter = 0;
			while (rez > tol) {																	//SEMI-IMPLICIT PM													
				for (int i = 1; i < 1 + h; i++) {
					for (int j = 1; j < 1 + w; j++) {
						sumGpq = g(normGrad(Ugrad[i * row + j][0], Ugrad[i * row + j][1])) + g(normGrad(Ugrad[i * row + j][2], Ugrad[i * row + j][3])) + g(normGrad(Ugrad[i * row + j][4], Ugrad[i * row + j][5])) + g(normGrad(Ugrad[i * row + j][6], Ugrad[i * row + j][7]));
						sumGpqUq = g(normGrad(Ugrad[i * row + j][0], Ugrad[i * row + j][1])) * Un[i * row + j + 1] + g(normGrad(Ugrad[i * row + j][2], Ugrad[i * row + j][3])) * Un[i * row + j - row] + g(normGrad(Ugrad[i * row + j][4], Ugrad[i * row + j][5])) * Un[i * row + j - 1] + g(normGrad(Ugrad[i * row + j][6], Ugrad[i * row + j][7])) * Un[i * row + j + row];

						y = (Uprev[i * row + j] + tau * sumGpqUq) / (double)(1 + tau * sumGpq);		//Gauss-Seidel
						newIntensity = SOR(y, Un[i * row + j], omega);								//SOR
						Un[i * row + j] = newIntensity;
					}
				}
				for (int j = 1; j < 1 + w; j++) {												//MIRRORING
					Un[j] = Un[j + row];														//top
					Un[(1 + h) * row + j] = Un[h * row + j];									//bottom

				}
				for (int i = 0; i < 2 + h; i++) {
					Un[i * row] = Un[i * row + 1];												//left
					Un[i * row + (1 + w)] = Un[i * row + w];									//right

				}
				rez = 0;
				for (int i = 1; i < 1 + h; i++) {												//CALCULATE RESIDUAL
					for (int j = 1; j < 1 + w; j++) {
						sumGpq = g(normGrad(Ugrad[i * row + j][0], Ugrad[i * row + j][1])) + g(normGrad(Ugrad[i * row + j][2], Ugrad[i * row + j][3])) + g(normGrad(Ugrad[i * row + j][4], Ugrad[i * row + j][5])) + g(normGrad(Ugrad[i * row + j][6], Ugrad[i * row + j][7]));
						sumGpqUq = g(normGrad(Ugrad[i * row + j][0], Ugrad[i * row + j][1])) * Un[i * row + j + 1] + g(normGrad(Ugrad[i * row + j][2], Ugrad[i * row + j][3])) * Un[i * row + j - row] + g(normGrad(Ugrad[i * row + j][4], Ugrad[i * row + j][5])) * Un[i * row + j - 1] + g(normGrad(Ugrad[i * row + j][6], Ugrad[i * row + j][7])) * Un[i * row + j + row];

						rezTMP = ((Uprev[i * row + j] + tau * sumGpqUq) / (double)(1 + tau * sumGpq)) - Un[i * row + j];
						rez += rezTMP * rezTMP;
					}
				}
				iter++;
				if (iter >= maxIter)
					break;
			}
			Uprev = Un;
		}
	}
	for (int i = 0; i < U0.size(); i++)
		U0[i] = round(Un[i] * 255.0);

	return U0;

}

QVector<int> Filters::semiImplicitMCF(QVector<int> U0, double tau, int n, int w, int h, int row, bool rgb)
{
	QVector<double> Un(U0.size());
	QVector<double> Uprev(U0.size());
	QMessageBox msgBox;
	double tol = 0.000001;
	double omega = 1.2;
	int iter, maxIter = 1000;
	double rezTMP, rez = 100;

	for (int i = 0; i < U0.size(); i++)
		Uprev[i] = U0[i] / 255.0;

	if (rgb) {
		//int sumUq_R, sumUq_G, sumUq_B;
		//double newIntensity_R, newIntensity_G, newIntensity_B;
	}
	else {
		double sumGpq;
		double newIntensity, y;
		QVector<double> Gpq(4);
		double GpVol, Ap, ApqUq;

		for (int step = 0; step < n; step++) {
			iter = 0;
			Un = Uprev;
			QVector< QVector <double> > Ugrad(U0.size());
			for (int i = 1; i < 1 + h; i++) {													//CALCULATE GRADIENTS
				for (int j = 1; j < 1 + w; j++) {
					Ugrad[i * row + j].resize(8);												//DIAMOND CELL
					Ugrad[i * row + j][0] = Uprev[i * row + j + 1] - Uprev[i * row + j];																					//west
					Ugrad[i * row + j][1] = (Uprev[(i - 1) * row + j] + Uprev[(i - 1) * row + j + 1] - Uprev[(i + 1) * row + j] - Uprev[(i + 1) * row + j + 1]) / 4.0;
					Ugrad[i * row + j][2] = (Uprev[(i - 1) * row + j - 1] + Uprev[i * row + j - 1] - Uprev[(i - 1) * row + j + 1] - Uprev[i * row + j + 1]) / 4.0;			//north
					Ugrad[i * row + j][3] = Uprev[(i - 1) * row + j] - Uprev[i * row + j];
					Ugrad[i * row + j][4] = Uprev[i * row + j - 1] - Uprev[i * row + j];																					//east
					Ugrad[i * row + j][5] = (Uprev[(i + 1) * row + j - 1] + Uprev[(i + 1) * row + j] - Uprev[(i - 1) * row + j - 1] - Uprev[(i - 1) * row + j]) / 4.0;
					Ugrad[i * row + j][6] = (Uprev[i * row + j + 1] + Uprev[(i + 1) * row + j + 1] - Uprev[i * row + j - 1] - Uprev[(i + 1) * row + j - 1]) / 4.0;			//south
					Ugrad[i * row + j][7] = Uprev[(i + 1) * row + j] - Uprev[i * row + j];
				}
			}
			while (rez > tol) {
				for (int i = 1; i < 1 + h; i++) {
					for (int j = 1; j < 1 + w; j++) {
						Gpq = { gMCF(normGrad(Ugrad[i * row + j][0], Ugrad[i * row + j][1])), gMCF(normGrad(Ugrad[i * row + j][2], Ugrad[i * row + j][3])), gMCF(normGrad(Ugrad[i * row + j][4], Ugrad[i * row + j][5])), gMCF(normGrad(Ugrad[i * row + j][6], Ugrad[i * row + j][7])) };
						sumGpq = Gpq[0] + Gpq[1] +Gpq[2] + Gpq[3];
						GpVol = 4.0 / sumGpq;
						Ap = (1 + tau * (GpVol*Gpq[0] + GpVol * Gpq[1] + GpVol * Gpq[2] + GpVol * Gpq[3] ));
						ApqUq = tau *( GpVol * Gpq[0] * Un[i * row + j + 1] + GpVol * Gpq[1] * Un[i * row + j - row] + GpVol * Gpq[2] * Un[i * row + j - 1] + GpVol * Gpq[3] * Un[i * row + j + row]);

						y = (Uprev[i * row + j] + ApqUq) / (double) Ap;							//Gauss-Seidel
						newIntensity = SOR(y, Un[i * row + j], omega);							//SOR
						Un[i * row + j] = newIntensity;
					}
				}																				//MIRRORING
				for (int j = 1; j < 1 + w; j++) {
					Un[j] = Un[j + row];														//top
					Un[(1 + h) * row + j] = Un[h * row + j];									//bottom
				}
				for (int i = 0; i < 2 + h; i++) {
					Un[i * row] = Un[i * row + 1];												//left
					Un[i * row + (1 + w)] = Un[i * row + w];									//right
				}
				rez = 0;
				for (int i = 1; i < 1 + h; i++) {
					for (int j = 1; j < 1 + w; j++) {
						Gpq = { gMCF(normGrad(Ugrad[i * row + j][0], Ugrad[i * row + j][1])), gMCF(normGrad(Ugrad[i * row + j][2], Ugrad[i * row + j][3])), gMCF(normGrad(Ugrad[i * row + j][4], Ugrad[i * row + j][5])), gMCF(normGrad(Ugrad[i * row + j][6], Ugrad[i * row + j][7])) };
						sumGpq = Gpq[0] + Gpq[1] + Gpq[2] + Gpq[3];
						GpVol = 4.0 / sumGpq;
						Ap = (1 + tau * (GpVol*Gpq[0] + GpVol * Gpq[1] + GpVol * Gpq[2] + GpVol * Gpq[3]));
						ApqUq = tau * (GpVol * Gpq[0] * Un[i * row + j + 1] + GpVol * Gpq[1] * Un[i * row + j - row] + GpVol * Gpq[2] * Un[i * row + j - 1] + GpVol * Gpq[3] * Un[i * row + j + row]);

						rezTMP = ((Uprev[i * row + j] + ApqUq) / (double) Ap) - Un[i * row + j];
						rez += rezTMP * rezTMP;
					}
				}
				iter++;
				if (iter >= maxIter)
					break;
			}
			Uprev = Un;
		}
	}
	for (int i = 0; i < U0.size(); i++)
		U0[i] = round(Un[i] * 255.0);

	return U0;
}

QVector<int> Filters::semiImplicitGMCF(QVector<int> U0, double sigma, double tau, int n, int w, int h, int row, bool rgb)
{
	QMessageBox msgBox;
	QVector<double> Un(U0.size());
	QVector<double> Uprev(U0.size());
	double tol = 0.000001;
	double omega = 1.2;
	int iter, maxIter = 1000;
	double rezTMP, rez = 100;
	double epsilon = 0.000001;
	//h = 1  
	int K = 1000;


	for (int i = 0; i < U0.size(); i++)															//intensity values to 0-1
		Uprev[i] = U0[i] / 255.0;

	if (rgb) {
		//int sumUq_R, sumUq_G, sumUq_B;
		//double newIntensity_R, newIntensity_G, newIntensity_B;
	}
	else {
		double sum, sumUq, UgradVol, Ap, ApqUq;
		double newIntensity, y;

		for (int step = 0; step < n; step++) {
			Un = Uprev;
			iter = 0;

			while (rez > tol)																	//IMPLICIT LHE
			{																																		
				for (int i = 1; i < 1 + h; i++) {
					for (int j = 1; j < 1 + w; j++) {
						sumUq = Un[i * row + j - 1] + Un[i * row + j + 1] + Un[i * row + j + row] + Un[i * row + j - row];
						y = (Uprev[i * row + j] + sigma * sumUq) / (double)(1 + 4 * sigma);		//Gauss-Seidel
						newIntensity = SOR(y, Un[i * row + j], omega);							//SOR
						Un[i * row + j] = newIntensity;
					}
				}
				for (int j = 1; j < 1 + w; j++) {												//MIRRORING
					Un[j] = Un[j + row];														//top
					Un[(1 + h) * row + j] = Un[h * row + j];									//bottom
				}
				for (int i = 0; i < 2 + h; i++) {
					Un[i * row] = Un[i * row + 1];												//left
					Un[i * row + (1 + w)] = Un[i * row + w];									//right
				}
				rez = 0;
				for (int i = 1; i < 1 + h; i++) {												//CALCULATE RESIDUAL
					for (int j = 1; j < 1 + w; j++) {
						sumUq = Un[i * row + j - 1] + Un[i * row + j + 1] + Un[i * row + j + row] + Un[i * row + j - row];
						rezTMP = (Uprev[i * row + j] + sigma * sumUq) / (double)(1 + 4 * sigma) - Un[i * row + j];
						rez += rezTMP * rezTMP;
					}
				}
				iter++;
				if (iter >= maxIter)
					break;
			}
			QVector< QVector <double> > Gpq(U0.size());
			QVector< QVector <double> > Ugrad(U0.size());
			for (int i = 1; i < 1 + h; i++) {													//CALCULATE GRADIENTS
				for (int j = 1; j < 1 + w; j++) {
					double Uw_x, Uw_y, Ue_x, Ue_y, Us_x, Us_y, Un_x, Un_y;
					Gpq[i * row + j].resize(4);
					Ugrad[i * row + j].resize(4);
					//DIAMOND CELL
					Uw_x = Un[i * row + j + 1] - Un[i * row + j];		//west
					Uw_y = (Un[(i - 1) * row + j] + Un[(i - 1) * row + j + 1] - Un[(i + 1) * row + j] - Un[(i + 1) * row + j + 1]) / 4.0;
					Un_y = Un[(i - 1) * row + j] - Un[i * row + j];		//north
					Un_x = (Un[(i - 1) * row + j - 1] + Un[i * row + j - 1] - Un[(i - 1) * row + j + 1] - Un[i * row + j + 1]) / 4.0;
					Ue_x = Un[i * row + j - 1] - Un[i * row + j];		//east
					Ue_y = (Un[(i + 1) * row + j - 1] + Un[(i + 1) * row + j] - Un[(i - 1) * row + j - 1] - Un[(i - 1) * row + j]) / 4.0;
					Us_y = Un[(i + 1) * row + j] - Un[i * row + j];		//south
					Us_x = (Un[i * row + j + 1] + Un[(i + 1) * row + j + 1] - Un[i * row + j - 1] - Un[(i + 1) * row + j - 1]) / 4.0;
					//gradient z predvyhladenych dat
					Gpq[i * row + j] = { g(normGrad(Uw_x, Uw_y)), g(normGrad(Un_x, Un_y)), g(normGrad(Ue_x, Ue_y)), g(normGrad(Us_x, Us_y)) };

					Uw_x = Uprev[i * row + j + 1] - Uprev[i * row + j];		//west
					Uw_y = (Uprev[(i - 1) * row + j] + Uprev[(i - 1) * row + j + 1] - Uprev[(i + 1) * row + j] - Uprev[(i + 1) * row + j + 1]) / 4.0;
					Un_y = Uprev[(i - 1) * row + j] - Uprev[i * row + j];	//north
					Un_x = (Uprev[(i - 1) * row + j - 1] + Uprev[i * row + j - 1] - Uprev[(i - 1) * row + j + 1] - Uprev[i * row + j + 1]) / 4.0;
					Ue_x = Uprev[i * row + j - 1] - Uprev[i * row + j];		//east
					Ue_y = (Uprev[(i + 1) * row + j - 1] + Uprev[(i + 1) * row + j] - Uprev[(i - 1) * row + j - 1] - Uprev[(i - 1) * row + j]) / 4.0;
					Us_y = Uprev[(i + 1) * row + j] - Uprev[i * row + j];	//south
					Us_x = (Uprev[i * row + j + 1] + Uprev[(i + 1) * row + j + 1] - Uprev[i * row + j - 1] - Uprev[(i + 1) * row + j - 1]) / 4.0;
					//gradient z povodnych dat, regularizovany
					Ugrad[i * row + j] = { gMCF(normGrad(Uw_x, Uw_y)), gMCF(normGrad(Un_x, Un_y)), gMCF(normGrad(Ue_x, Ue_y)), gMCF(normGrad(Us_x, Us_y)) };
				}
			}
			Un = Uprev;
			rez = 100;
			iter = 0;
			while (rez > tol) {																	//GMCF													
				for (int i = 1; i < 1 + h; i++) {
					for (int j = 1; j < 1 + w; j++) {
						sum = Ugrad[i * row + j][0] + Ugrad[i * row + j][1] + Ugrad[i * row + j][2] + Ugrad[i * row + j][3];
						UgradVol = 4.0 / sum;
						Ap = (1 + tau * UgradVol * (Gpq[i * row + j][0]* Ugrad[i * row + j][0] + Gpq[i * row + j][1] * Ugrad[i * row + j][1] + Gpq[i * row + j][2] * Ugrad[i * row + j][2] +  Gpq[i * row + j][3] * Ugrad[i * row + j][3]));
						ApqUq = tau * UgradVol * 
							(Gpq[i * row + j][0] * Ugrad[i * row + j][0] * Un[i * row + j + 1] 
							+ Gpq[i * row + j][1] * Ugrad[i * row + j][1] * Un[i * row + j - row] 
							+ Gpq[i * row + j][2] * Ugrad[i * row + j][2] * Un[i * row + j - 1] 
							+ Gpq[i * row + j][3] * Ugrad[i * row + j][3] * Un[i * row + j + row]);

						y = (Uprev[i * row + j] + ApqUq) / (double)Ap;							//Gauss-Seidel
						newIntensity = SOR(y, Un[i * row + j], omega);							//SOR
						//msgBox.setText("old: " + QString::number(Un[i*row + j]) + "  new: " + QString::number(newIntensity));
						//msgBox.exec();
						Un[i * row + j] = newIntensity;
					}
				}																				//MIRRORING
				for (int j = 1; j < 1 + w; j++) {
					Un[j] = Un[j + row];														//top
					Un[(1 + h) * row + j] = Un[h * row + j];									//bottom
				}
				for (int i = 0; i < 2 + h; i++) {
					Un[i * row] = Un[i * row + 1];												//left
					Un[i * row + (1 + w)] = Un[i * row + w];									//right
				}
				rez = 0;
				for (int i = 1; i < 1 + h; i++) {
					for (int j = 1; j < 1 + w; j++) {
						sum = Ugrad[i * row + j][0] + Ugrad[i * row + j][1] + Ugrad[i * row + j][2] + Ugrad[i * row + j][3];
						UgradVol = 4.0 / sum;
						Ap = (1 + tau * UgradVol * (Gpq[i * row + j][0] * Ugrad[i * row + j][0] + Gpq[i * row + j][1] * Ugrad[i * row + j][1] + Gpq[i * row + j][2] * Ugrad[i * row + j][2] + Gpq[i * row + j][3] * Ugrad[i * row + j][3]));
						ApqUq = tau * UgradVol *
							(Gpq[i * row + j][0] * Ugrad[i * row + j][0] * Un[i * row + j + 1]
								+ Gpq[i * row + j][1] * Ugrad[i * row + j][1] * Un[i * row + j - row]
								+ Gpq[i * row + j][2] * Ugrad[i * row + j][2] * Un[i * row + j - 1]
								+ Gpq[i * row + j][3] * Ugrad[i * row + j][3] * Un[i * row + j + row]);

						rezTMP = ((Uprev[i * row + j] + ApqUq) / (double)Ap) - Un[i * row + j];
						rez += rezTMP * rezTMP;
					}
				}
				iter++;
				if (iter >= maxIter)
					break;
			}
			Uprev = Un;
		}
	}
	for (int i = 0; i < U0.size(); i++)
		U0[i] = round(Un[i] * 255.0);

	return U0;
}

QVector<int> Filters::subsurf(QVector<int> U0, double sigma, double tau, int n, int w, int h, int row, bool rgb)
{
	QMessageBox msgBox;
	QVector<double> Un(U0.size());
	QVector<double> Uprev(U0.size());
	QVector<double> Udist(U0.size());
	double tol = 0.000001;
	double omega = 1.2;
	int iter, maxIter = 1000;
	double rezTMP, rez = 100;
	double epsilon = 0.000001;
	//h = 1  
	int K = 1000;

	
	for (int i = 0; i < U0.size(); i++)															//intensity values to 0-1
		Uprev[i] = U0[i] / 255.0;

	Udist = signedDistFunction(U0,ruoyTourin(U0,0.4,w,h,row),w,h,row);

	if (rgb) {
		//int sumUq_R, sumUq_G, sumUq_B;
		//double newIntensity_R, newIntensity_G, newIntensity_B;
	}
	else {
		double sum, sumUq, UgradVol, Ap, ApqUq;
		double newIntensity, y;

		for (int step = 0; step < n; step++) {
			Un = Uprev;
			iter = 0;

			while (rez > tol)																	//IMPLICIT LHE
			{
				for (int i = 1; i < 1 + h; i++) {
					for (int j = 1; j < 1 + w; j++) {
						sumUq = Un[i * row + j - 1] + Un[i * row + j + 1] + Un[i * row + j + row] + Un[i * row + j - row];
						y = (Uprev[i * row + j] + sigma * sumUq) / (double)(1 + 4 * sigma);		//Gauss-Seidel
						newIntensity = SOR(y, Un[i * row + j], omega);							//SOR
						Un[i * row + j] = newIntensity;
					}
				}
				for (int j = 1; j < 1 + w; j++) {												//MIRRORING
					Un[j] = Un[j + row];														//top
					Un[(1 + h) * row + j] = Un[h * row + j];									//bottom
				}
				for (int i = 0; i < 2 + h; i++) {
					Un[i * row] = Un[i * row + 1];												//left
					Un[i * row + (1 + w)] = Un[i * row + w];									//right
				}
				rez = 0;
				for (int i = 1; i < 1 + h; i++) {												//CALCULATE RESIDUAL
					for (int j = 1; j < 1 + w; j++) {
						sumUq = Un[i * row + j - 1] + Un[i * row + j + 1] + Un[i * row + j + row] + Un[i * row + j - row];
						rezTMP = (Uprev[i * row + j] + sigma * sumUq) / (double)(1 + 4 * sigma) - Un[i * row + j];
						rez += rezTMP * rezTMP;
					}
				}
				iter++;
				if (iter >= maxIter)
					break;
			}
			QVector< QVector <double> > Gpq(U0.size());
			QVector< QVector <double> > Ugrad(U0.size());
			for (int i = 1; i < 1 + h; i++) {													//CALCULATE GRADIENTS
				for (int j = 1; j < 1 + w; j++) {
					double Uw_x, Uw_y, Ue_x, Ue_y, Us_x, Us_y, Un_x, Un_y;
					Gpq[i * row + j].resize(4);
					Ugrad[i * row + j].resize(4);
					//DIAMOND CELL
					Uw_x = Un[i * row + j + 1] - Un[i * row + j];		//west
					Uw_y = (Un[(i - 1) * row + j] + Un[(i - 1) * row + j + 1] - Un[(i + 1) * row + j] - Un[(i + 1) * row + j + 1]) / 4.0;
					Un_y = Un[(i - 1) * row + j] - Un[i * row + j];		//north
					Un_x = (Un[(i - 1) * row + j - 1] + Un[i * row + j - 1] - Un[(i - 1) * row + j + 1] - Un[i * row + j + 1]) / 4.0;
					Ue_x = Un[i * row + j - 1] - Un[i * row + j];		//east
					Ue_y = (Un[(i + 1) * row + j - 1] + Un[(i + 1) * row + j] - Un[(i - 1) * row + j - 1] - Un[(i - 1) * row + j]) / 4.0;
					Us_y = Un[(i + 1) * row + j] - Un[i * row + j];		//south
					Us_x = (Un[i * row + j + 1] + Un[(i + 1) * row + j + 1] - Un[i * row + j - 1] - Un[(i + 1) * row + j - 1]) / 4.0;
					//gradient z predvyhladenych dat
					Gpq[i * row + j] = { g(normGrad(Uw_x, Uw_y)), g(normGrad(Un_x, Un_y)), g(normGrad(Ue_x, Ue_y)), g(normGrad(Us_x, Us_y)) };

					Uw_x = Udist[i * row + j + 1] - Udist[i * row + j];		//west
					Uw_y = (Udist[(i - 1) * row + j] + Udist[(i - 1) * row + j + 1] - Udist[(i + 1) * row + j] - Udist[(i + 1) * row + j + 1]) / 4.0;
					Un_y = Udist[(i - 1) * row + j] - Udist[i * row + j];	//north
					Un_x = (Udist[(i - 1) * row + j - 1] + Udist[i * row + j - 1] - Udist[(i - 1) * row + j + 1] - Udist[i * row + j + 1]) / 4.0;
					Ue_x = Udist[i * row + j - 1] - Udist[i * row + j];		//east
					Ue_y = (Udist[(i + 1) * row + j - 1] + Udist[(i + 1) * row + j] - Udist[(i - 1) * row + j - 1] - Udist[(i - 1) * row + j]) / 4.0;
					Us_y = Udist[(i + 1) * row + j] - Udist[i * row + j];	//south
					Us_x = (Udist[i * row + j + 1] + Udist[(i + 1) * row + j + 1] - Udist[i * row + j - 1] - Udist[(i + 1) * row + j - 1]) / 4.0;
					//gradient z distancnej funkcie, regularizovany
					Ugrad[i * row + j] = { gMCF(normGrad(Uw_x, Uw_y)), gMCF(normGrad(Un_x, Un_y)), gMCF(normGrad(Ue_x, Ue_y)), gMCF(normGrad(Us_x, Us_y)) };
				}
			}
			Un = Udist;
			rez = 100;
			iter = 0;
			while (rez > tol) {																	//SUBSURF												
				for (int i = 1; i < 1 + h; i++) {
					for (int j = 1; j < 1 + w; j++) {
						sum = Ugrad[i * row + j][0] + Ugrad[i * row + j][1] + Ugrad[i * row + j][2] + Ugrad[i * row + j][3];
						UgradVol = 4.0 / sum;
						Ap = (1 + tau * UgradVol * (Gpq[i * row + j][0] * Ugrad[i * row + j][0] + Gpq[i * row + j][1] * Ugrad[i * row + j][1] + Gpq[i * row + j][2] * Ugrad[i * row + j][2] + Gpq[i * row + j][3] * Ugrad[i * row + j][3]));
						ApqUq = tau * UgradVol *
							(Gpq[i * row + j][0] * Ugrad[i * row + j][0] * Un[i * row + j + 1]
								+ Gpq[i * row + j][1] * Ugrad[i * row + j][1] * Un[i * row + j - row]
								+ Gpq[i * row + j][2] * Ugrad[i * row + j][2] * Un[i * row + j - 1]
								+ Gpq[i * row + j][3] * Ugrad[i * row + j][3] * Un[i * row + j + row]);

						y = (Udist[i * row + j] + ApqUq) / (double)Ap;							//Gauss-Seidel
						newIntensity = SOR(y, Un[i * row + j], omega);							//SOR
						//msgBox.setText("old: " + QString::number(Un[i*row + j]) + "  new: " + QString::number(newIntensity));
						//msgBox.exec();
						Un[i * row + j] = newIntensity;
					}
				}																				//MIRRORING
				for (int j = 1; j < 1 + w; j++) {
					Un[j] = Un[j + row];														//top
					Un[(1 + h) * row + j] = Un[h * row + j];									//bottom
				}
				for (int i = 0; i < 2 + h; i++) {
					Un[i * row] = Un[i * row + 1];												//left
					Un[i * row + (1 + w)] = Un[i * row + w];									//right
				}
				rez = 0;
				for (int i = 1; i < 1 + h; i++) {
					for (int j = 1; j < 1 + w; j++) {
						sum = Ugrad[i * row + j][0] + Ugrad[i * row + j][1] + Ugrad[i * row + j][2] + Ugrad[i * row + j][3];
						UgradVol = 4.0 / sum;
						Ap = (1 + tau * UgradVol * (Gpq[i * row + j][0] * Ugrad[i * row + j][0] + Gpq[i * row + j][1] * Ugrad[i * row + j][1] + Gpq[i * row + j][2] * Ugrad[i * row + j][2] + Gpq[i * row + j][3] * Ugrad[i * row + j][3]));
						ApqUq = tau * UgradVol *
							(Gpq[i * row + j][0] * Ugrad[i * row + j][0] * Un[i * row + j + 1]
								+ Gpq[i * row + j][1] * Ugrad[i * row + j][1] * Un[i * row + j - row]
								+ Gpq[i * row + j][2] * Ugrad[i * row + j][2] * Un[i * row + j - 1]
								+ Gpq[i * row + j][3] * Ugrad[i * row + j][3] * Un[i * row + j + row]);

						rezTMP = ((Udist[i * row + j] + ApqUq) / (double)Ap) - Un[i * row + j];
						rez += rezTMP * rezTMP;
					}
				}
				iter++;
				if (iter >= maxIter)
					break;
			}
			Udist = Un;
		}
	}
	for (int i = 0; i < U0.size(); i++)
		U0[i] = round(Un[i] * 255.0);

	return U0;
}

QVector<int> Filters::contour(QVector<int> U0, int w, int h, int row)
{
	QVector<int> Un = U0;

	for (int i = 1; i < h + 1; i++)
		for (int j = 1; j < w + 1; j++) {
			if (U0[i * row + j] == 0 && (U0[i * row + j - 1] == 255 || U0[i * row + j + 1] == 255 || U0[(i - 1) * row + j] == 255 || U0[(i + 1) * row + j] == 255)) {
				Un[i * row + j] = 255;
				//Un[i * row + j - 1] = 0;
				//Un[i * row + j + 1] = 0;
				//Un[(i - 1) * row + j] = 0;
				//Un[(i + 1) * row + j] = 0;
			}
			else
				Un[i * row + j] = 0;
		}

	return Un;
}

QVector<int> Filters::colourInside(QVector<int> U0, int w, int h, int row, int intensity)
{
	QVector<int> Un = U0;
	bool inside = false;

	for (int i = 1; i < h + 1; i++) {
		inside = false;
		for (int j = 1; j < w + 1; j++) {
			if (U0[i * row + j] == 0 && inside == false) {
				inside = true;
				while (U0[i * row + j] == 0)
					j++;
			}
			if (inside == true) {
				Un[i * row + j] = intensity;
			}
			if (U0[i * row + j] == 0 && inside == true) {
				inside = false;
				while (U0[i * row + j] == 0)
					j++;
			}
		}
	}

	return Un;
}

QVector<double> Filters::ruoyTourin(QVector<int> U0, double tau, int w, int h, int row)
{
	QVector<double> Un(U0.size());
	QVector<double> Uprev(U0.size());
	for (int i = 0; i < U0.size(); i++) {														//intensity values to 0-1
		Uprev[i] = U0[i] / 255.0;
		//printf("Uprev: %lf \t ", Uprev[i]);
	}
	Un = Uprev;

	double mass = 10.;
	int iter = 1, maxIter = 1000;
	double tol = 0.0000001;
	
	while (mass > tol) {
		for (int i = 1; i < h + 1; i++)
			for (int j = 1; j < w + 1; j++) {
				if (U0[i * row + j] != 255) {
					Un[i * row + j] = Uprev[i * row + j] + tau - tau * 
						sqrt(
							max(M(Uprev[i * row + j], Uprev[(i - 1) * row + j]),M(Uprev[i * row + j], Uprev[(i + 1) * row + j])) +
							max(M(Uprev[i * row + j], Uprev[i * row + j - 1]), M(Uprev[i * row + j], Uprev[i * row + j + 1]))
						);
				}
			}
		//MIRRORING
		for (int j = 1; j < 1 + w; j++) {
			Un[j] = Un[j + row];														//top
			Un[(1 + h) * row + j] = Un[h * row + j];									//bottom
		}
		for (int i = 0; i < 2 + h; i++) {
			Un[i * row] = Un[i * row + 1];												//left
			Un[i * row + (1 + w)] = Un[i * row + w];									//right
		}
		mass = 0.;
		for (int i = 1; i < h + 1; i++)
			for (int j = 1; j < w + 1; j++)
				mass += (Uprev[i * row + j] - Un[i * row + j]) * (Uprev[i * row + j] - Un[i * row + j]);
		mass = sqrt(mass);
		printf("Iteration: %d	mass: %lf \n",iter,mass);
		iter++;
		if (iter >= maxIter)
			break;
		Uprev = Un;
	}

	return Un;
}

QVector<double> Filters::signedDistFunction(QVector<int> U0, QVector<double> Udist, int w, int h, int row)
{
	QVector<double> Un(U0.size());

	for (int i = 1; i < h + 1; i++)
		for (int j = 1; j < w + 1; j++) {
			if (U0[i * row + j] == 255)
				Un[i * row + j] = (1.0 * Udist[i * row + j]);
			else
				Un[i * row + j] = (-1.0 * Udist[i * row + j]);
		}
	return Un;
}

double Filters::SOR(double y, double UpOld, double omega)
{
	double UpNew;

	UpNew = UpOld + omega * (y - UpOld);
	return UpNew;
}

double Filters::g(double s, double K)
{
	//vstupuje stvorec normy gradientu
	
	return 1.0 / (1 + K*s);
}

double Filters::gMCF(double s)
{
	//vstupuje stvorec normy gradientu
	double epsilon = 0.000001;
	return 1 / sqrt(s + epsilon);
}

double Filters::normGrad(double U_x, double U_y)
{
	return U_x*U_x + U_y*U_y;
}

double Filters::min(double a, double b)
{
	if (a <= b)
		return a;
	else
		return b;
}

double Filters::max(double a, double b)
{
	if (a >= b)
		return a;
	else
		return b;
}

double Filters::M(double a, double b)
{
	double m = min(b - a, 0.);

	return m * m;
}
