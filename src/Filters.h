#pragma once
#include <QVector>
#include <QMessageBox>
class Filters
{
public:
	Filters();
	~Filters();
	QVector<int> explicitLinDif(QVector<int> U0, double tau, int n,int w, int h, int row, bool rgb);
	QVector<int> implicitLinDif(QVector<int> U0, double tau, int n, int w, int h, int row, bool rgb);
	QVector<int> explicitPM(QVector<int> U0, double tau, int n, int w, int h, int row, bool rgb);
	QVector<int> semiImplicitPM(QVector<int> U0, double tau, int n, int w, int h, int row, bool rgb);
	QVector<int> semiImplicitRegPM(QVector<int> U0,double sigma, double tau, int n, int w, int h, int row, bool rgb);
	QVector<int> semiImplicitMCF(QVector<int> U0, double tau, int n, int w, int h, int row, bool rgb);
	QVector<int> semiImplicitGMCF(QVector<int> U0, double sigma, double tau, int n, int w, int h, int row, bool rgb);
	QVector<int> subsurf(QVector<int> U0, double sigma, double tau, int n, int w, int h, int row, bool rgb);
	QVector<int> contour(QVector<int> U0, int w, int h, int row);
	QVector<int> colourInside(QVector<int> U0, int w, int h, int row,int intensity);
	QVector<double> ruoyTourin(QVector<int> U0, double tau,int w, int h, int row);
	QVector<double> signedDistFunction(QVector<int> U0, QVector<double> Udist, int w, int h, int row);
	double SOR(double y, double UpOld, double omega);
	double g(double s, double K = 1);
	double gMCF(double s);
	double normGrad(double U_x, double U_y);
	double min(double a, double b);
	double max(double a, double b);
	double M(double a, double b);
};

