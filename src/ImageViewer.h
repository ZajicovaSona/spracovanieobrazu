#pragma once

#include <QtWidgets/QMainWindow>
#include <QCoreApplication>
#include "ui_ImageViewer.h"
#include "ViewerWidget.h"
#include "Filters.h"
#include "NewImageDialog.h"
#include <iostream>

class ImageViewer : public QMainWindow
{
	Q_OBJECT

public:
	ImageViewer(QWidget* parent = Q_NULLPTR);

private:
	Ui::ImageViewerClass* ui;
	NewImageDialog* newImgDialog;
	QSettings settings;
	QMessageBox msgBox;
	int tabCount = 0;

	//ViewerWidget functions
	ViewerWidget* getViewerWidget(int tabId);
	ViewerWidget* getCurrentViewerWidget();

	//Event filters
	bool eventFilter(QObject* obj, QEvent* event);

	//ViewerWidget Events
	bool ViewerWidgetEventFilter(QObject* obj, QEvent* event);
	void ViewerWidgetMouseButtonPress(ViewerWidget* w, QEvent* event);
	void ViewerWidgetMouseButtonRelease(ViewerWidget* w, QEvent* event);
	void ViewerWidgetMouseMove(ViewerWidget* w, QEvent* event);
	void ViewerWidgetLeave(ViewerWidget* w, QEvent* event);
	void ViewerWidgetEnter(ViewerWidget* w, QEvent* event);
	void ViewerWidgetWheel(ViewerWidget* w, QEvent* event);

	//ImageViewer Events
	void closeEvent(QCloseEvent* event);

	//Image functions
	void openNewTabForImg(ViewerWidget* vW);
	bool openImage(QString filename);
	bool saveImage(QString filename);
	bool clearImage();
	bool invertColors();
	QVector<int> mirroring(int k);
	bool FSHS();
	bool convolution(int size, QList<double> mask);
	bool BHThreshold();
	bool OtsuThreshold();
	bool adaptiveThreshold();

	//Inline functions
	inline bool isImgOpened() { return ui->tabWidget->count() == 0 ? false : true; }

private slots:
	//Tabs slots
	void on_tabWidget_tabCloseRequested(int tabId);
	void on_actionRename_triggered();
	void tab_changed(int i);

	//Image slots
	void on_actionNew_triggered();
	void newImageAccepted();
	void on_actionOpen_triggered();
	void on_actionSave_as_triggered();
	void on_actionClear_triggered();
	void on_actionInvert_colors_triggered();
	void FSHS_clicked();
	void convolution_clicked();
	void histThresholding_clicked();
	void otsuThresholding_clicked();
	void adaptThresholding_clicked();
	void mirrorHalf_clicked();
	void mirrorFull_clicked();
	void mirrorQuarter_clicked();

	//Tools slots
	void showTools_clicked();
	void hideTools_clicked();

	//Filter slots
	void explicitLinDif_clicked();
	void implicitLinDif_clicked();
	void explicitPM_clicked();
	void semiImplicitPM_clicked();
	void semiImplicitRegPM_clicked();
	void semiImplicitMCF_clicked();
	void semiImplicitGMCF_clicked();
	void distanceFunction_clicked();
	void signedDistFunction_clicked();
	void subsurf_clicked();
	void slider_moved(int i);
};
